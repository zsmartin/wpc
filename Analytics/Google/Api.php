<?php

namespace WPC\Analytics\Google;

use \WPC\Component;

class Api extends Component
{
    const TOKEN_OPTION_KEY = 'ga_auth_token';

    public static $REPORT_PARAM_DEFAULTS = array(
        'type' => 'trending',
        'from' => '30daysAgo',
        'to' => 'today',
        'limit' => 100,
        'post_filter' => null
    );

    public $account;
    public $property;
    public $profile;
    public $applicationName;
    public $clientId;
    public $clientSecret;
    public $redirectUri;
    public $apiKey;

    protected $authToken = '';
    protected $service;
    protected $accountId = null;
    protected $propertyId = null;
    protected $profileId = null;
    protected $client;

    public function init()
    {
        require_once LIBRARY_PATH . '/GoogleAnalytics/Google_Client.php';
        require_once LIBRARY_PATH . '/GoogleAnalytics/contrib/Google_AnalyticsService.php';

        $this->client = new \Google_Client();
        $this->client->setApplicationName($this->applicationName);
        $this->client->setClientId($this->clientId);
        $this->client->setClientSecret($this->clientSecret);
        $this->client->setRedirectUri(site_url($this->redirectUri));
        $this->client->setDeveloperKey($this->apiKey);
        $this->client->setScopes(array('https://www.googleapis.com/auth/analytics.readonly'));
        $this->client->setUseObjects(true);

        $this->service = new \Google_AnalyticsService($this->client);

        $this->loadAuthToken();

        if (!strlen($this->authToken)) {
            return;
        }

        $this->client->setAccessToken($this->authToken);
    }

    public function getAuthUrl()
    {
        return $this->client->createAuthUrl();
    }

    public function authenticate($authCode)
    {
        $_GET['code'] = $authCode;
        if ($this->client->authenticate()) {
            $this->saveAuthToken($this->client->getAccessToken());
            if (!$this->loadAuthToken()) {
                return false;
            }
        } else {
            return false;
        }

        return true;
    }

    public function isAuthenticated()
    {
        if ($this->loadAuthToken()) {
            $this->client->setAccessToken($this->authToken);
            return true;
        } else {
            return false;
        }
    }

    public function saveAuthToken($token)
    {
        update_option(self::TOKEN_OPTION_KEY, $token);
    }

    public function loadAuthToken()
    {
        $authToken = get_option(self::TOKEN_OPTION_KEY);
        if ($authToken) {
            $this->authToken = $authToken;
        }

        return strlen($this->authToken) > 0;
    }

    protected function getAnalyticsManagementId($type)
    {
        switch ($type) {
            case 'account':
                $name = $this->account;
                $accounts = $this->service->management_accounts->listManagementAccounts();
                $items = $accounts->getItems();
                break;
            case 'property':
                if (!$this->getAccountId()) {
                    return false;
                }
                $name = $this->property;
                $properties = $this->service->management_webproperties->listManagementWebproperties($this->getAccountId());
                $items = $properties->getItems();
                break;
            case 'profile':
                if (!$this->getPropertyId()) {
                    return false;
                }
                $name = $this->profile;
                $profiles = $this->service->management_profiles->listManagementProfiles($this->getAccountId(), $this->getPropertyId());
                $items = $profiles->getItems();
                break;
        }

        if (!count($items)) {
            throw new \Exception("No {$type} found for this user.");
        }

        while (count($items)) {
            if ($items[0]->getName() === $name) {
                break;
            }
            array_shift($items);
        }

        if (!count($items)) {
            throw new \Exception("No {$type} found by name {$name}");
        }

        return $items[0]->getId();
    }

    public function getAccountId()
    {
        if (isset($this->accountId)) {
            return $this->accountId;
        }

        try {
            $result = $this->getAnalyticsManagementId('account');
            if ($result) {
                $this->accountId = $result;
            }
            return $result;
        } catch (Exception $e) {
            echo $e->getFile . ':' . $e->getLine . "\t" . $e->getMessage() . "\n";
            return false;
        }
    }

    public function getPropertyId()
    {
        if (isset($this->propertyId)) {
            return $this->propertyId;
        }

        try {
            $result = $this->getAnalyticsManagementId('property');
            if ($result) {
                $this->propertyId = $result;
            }
            return $result;
        } catch (Exception $e) {
            echo $e->getFile . ':' . $e->getLine . "\t" . $e->getMessage() . "\n";
            return false;
        }
    }

    public function getProfileId()
    {
        if (isset($this->profileId)) {
            return $this->profileId;
        }

        try {
            $result = $this->getAnalyticsManagementId('profile');
            if ($result) {
                $this->profileId = $result;
            }
            return $result;
        } catch (Exception $e) {
            echo $e->getFile . ':' . $e->getLine . "\t" . $e->getMessage() . "\n";
            return false;
        }
    }

    public function getPosts($type, $from, $to, $context = null)
    {
        if (!$this->getProfileId()) {
            throw new \Exception('Could not retrieve profile id');
        }

        switch ($type) {
            case 'trending':
                $metrics = 'ga:pageviews,ga:uniquePageviews,ga:timeOnPage,ga:bounces,ga:entrances,ga:exits';
                $options = array('dimensions' => 'ga:pagePath', 'sort' => '-ga:pageviews');
                break;
            case 'path':
                foreach ($context['paths'] as &$path) {
                    $path = 'ga:pagePath==' . $path;
                }
                $pathString = implode(',', $context['paths']);
                $metrics = 'ga:pageviews, ga:uniquePageviews';
                $options = array(
                    'dimensions' => 'ga:pagePath',
                    'filters' => $pathString
                );
                break;
        }

        $results = $this->service->data_ga->get(
                'ga:' . $this->getProfileId(), $from, $to, $metrics, $options);

        if (count($results->getRows()) > 0) {
            return $results->getRows();
        } else {
            throw new \Exception('No results found');
        }
    }

    public static function normalizeReportParams(array $params)
    {
        return array_merge(self::$REPORT_PARAM_DEFAULTS, $params);
    }

    public static function getCacheKey(array $params)
    {
        return "ga-{$params['key']}";
    }

    public function createReport(array $params)
    {
        $params = self::normalizeReportParams($params);

        $gaData = $this->getPosts($params['type'], $params['from'], $params['to']);

        $urls = $this->gaDataToUrl($gaData);
        $posts = $this->processLinks($urls, $params['limit'], $params['post_filter']);

        App()->getCache()->set(self::getCacheKey($params), $posts);
        return $posts;
    }

    public function getReport(array $params)
    {
        $report = App()->getCache()->get(self::getCacheKey($params));
        if (false === $report) {
            $report = $this->createReport($params);
        }

        return $report;
    }

    protected function gaDataToUrl($gaData)
    {
        $result = array();
        foreach ($gaData as $data) {
            $result[] = $data[0];
        }

        return $result;
    }

    protected function processLinks(array $urls, $limit = 100, $postFilter = null)
    {
        $posts = array();
        foreach ($urls as $url) {
            if (strlen($url) < 2) {
                continue;
            }

            //query paraméterek levágása
            $permalink = preg_replace('@\?.*$@', '', site_url($url));
            //több oldalas cikkek szűrése
            $permalink = preg_replace('@/[0-9]+/$@', '/', $permalink);
            $postId = url_to_postid($permalink);
            if (!$postId) {
                continue;
            }

            /** @var WP_Post */
            $post = get_post($postId);
            if (!$post || $post->post_type !== 'post') {
                continue;
            }

            if (is_callable($postFilter)) {
                $post = call_user_func($postFilter, $post);
            }

            if (!$post) {
                continue;
            }
            $posts[$post->ID] = $post;
            if (count($posts) >= $limit) {
                break;
            }
        }

        return $posts;
    }

    public static function setupOauthCallback()
    {
        add_filter('rewrite_rules_array', function($rules) {
            $newRules = array(
                'api/analytics/oauth-callback/(success|failed)/?$' => 'index.php?ga_oauth_callback=1&ga_oauth_result=$matches[1]',
                'api/analytics/oauth-callback/?$' => 'index.php?ga_oauth_callback=1'
            );
            return $newRules + $rules;
        }, 20);

        add_filter('query_vars', function($vars) {
            array_push($vars, 'ga_oauth_callback', 'ga_oauth_result', 'ga_oauth_token');
            return $vars;
        });

        add_action('parse_query', function($wpQuery) {
            if ((int)$wpQuery->get('ga_oauth_callback') === 1) {
                require_once(FRAMEWORK_PATH . '/Analytics/Google/oauth2callback.php');
                die();
            }
        });
    }
}