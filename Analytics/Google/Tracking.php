<?php

namespace WPC\Analytics\Google;

use \WPC\Component;

class Tracking extends Component
{
    public $trackingId;

    public function init()
    {
        add_action('init', function(){
            if (!is_array($this->trackingId)) {
                $this->trackingId = array(
                    'default' => $this->trackingId
                );
            }

            $this->addTrackingCode();
            if (is_single()) {
                $this->addAuthorMeta();
            }
        });
    }

    public function addTrackingCode()
    {
        $self = $this;
        add_action('wp_head', function() use ($self) {
            ?>
<script>
window.GoogleAnalyticsTrackingIds = <?php echo json_encode($this->trackingId); ?>;

(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

<?php foreach ($this->trackingId as $name => $id): ?>
    <?php if ($name === 'default'): ?>
    ga('create', '<?php echo $id; ?>', 'auto');
    ga('require', 'displayfeatures');
    ga('send', 'pageview');
    <?php else: ?>
    ga('create', '<?php echo $id; ?>', 'auto', {'name': '<?php echo $name; ?>'});
    ga('<?php echo $name; ?>.send', 'pageview');
    <?php endif; ?>
<?php endforeach; ?>
</script>
            <?php
        }, 100);
    }

    public function addAuthorMeta()
    {
        $googleAuthor = get_the_author_meta('website');
        if ($googleAuthor) {
            add_action('wp_head', function() use ($googleAuthor) {
                echo '<link rel="author" href="' . $googleAuthor . '">' . PHP_EOL;
            });
        }
    }
}