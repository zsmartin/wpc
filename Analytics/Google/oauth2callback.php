<?php
if ($result = get_query_var('ga_oauth_result')) {
    $message = $result === 'success' ? 'Sikeres autentikáció!' : 'Sikertelen autentikáció!';
?>
<h1><?php echo $message; ?></h1>
<?php
    die();
}

if (!isset($_GET['code'])) {
    wp_redirect(site_url());
    die();
}

try {
    $analytics = App()->getAnalytics()->getApi();
    $authResult = $analytics->client->authenticate();
} catch (\Google_Exception $e) {
    echo $e->getMessage();
    die();
}

if ($authResult) {
    $analytics->saveAuthToken($analytics->client->getAccessToken());
    wp_redirect(site_url('/api/analytics/oauth-callback/success'));
    die();
} else {
    wp_redirect(site_url('/api/analytics/oauth-callback/failed'));
    die();
}