<?php

namespace WPC\Analytics;

use \WPC\Component,
    \WPC\Analytics\Google;

class Google extends Component
{
    public $api;
    public $trackingId;

    protected $_api;

    public function init()
    {
        new Google\Tracking(array('trackingId' => $this->trackingId));
        Google\Api::setupOauthCallback();
    }

    /**
     *
     * @return \WPC\Analytics\Google\Api
     */
    public function getApi()
    {
        if (!is_null($this->_api)) {
            return $this->_api;
        }

        $this->_api = new Google\Api($this->api);

        return $this->_api;
    }
}