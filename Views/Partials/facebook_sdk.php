<div id="fb-root"></div>
<script>
    window.fbAsyncInit = function() {
        FB.init({
          appId : "<?php echo $appId; ?>",
          status : true,
          cookie : true,
          xfbml: true
        });
        FB.getLoginStatus(function(response){
            jQuery(document).trigger("facebook.init");
            jQuery(document).trigger("facebook.loginStatus", response);
        });
    };
    (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id; js.async = true;
        js.src = "//connect.facebook.net/hu_HU/all.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>