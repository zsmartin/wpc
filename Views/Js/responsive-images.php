/*! matchMedia() polyfill - Test a CSS media type/query in JS. Authors & copyright (c) 2012: Scott Jehl, Paul Irish, Nicholas Zakas. Dual MIT/BSD license */
/* vim:set shiftwidth=4 expandtab tabstop=4: */

window.matchMedia = window.matchMedia || (function( doc, undefined ) {

  "use strict";

  var bool,
      docElem = doc.documentElement,
      refNode = docElem.firstElementChild || docElem.firstChild,
      // fakeBody required for <FF4 when executed in <head>
      fakeBody = doc.createElement( "body" ),
      div = doc.createElement( "div" );

  div.id = "mq-test-1";
  div.style.cssText = "position:absolute;top:-100em";
  fakeBody.style.background = "none";
  fakeBody.appendChild(div);

  return function(q){

    div.innerHTML = "&shy;<style media=\"" + q + "\"> #mq-test-1 { width: 42px; }</style>";

    docElem.insertBefore( fakeBody, refNode );
    bool = div.offsetWidth === 42;
    docElem.removeChild( fakeBody );

    return {
      matches: bool,
      media: q
    };

  };

}( document ));

jQuery(document).ready(function($){
    $('[data-type="responsive-background"]').responsiveBackgrounds(true);
    $('.responsive-image-container').responsiveImages(true);

    $(window).resize(function(){
        $('[data-type="responsive-background"]').responsiveBackgrounds(true);
        $('.responsive-image-container').responsiveImages(true);
    });

    $(window).scroll(function(){
        $('[data-type="responsive-background"]').responsiveBackgrounds(false);
        $('.responsive-image-container').responsiveImages(false);
    });
});

(function($){
    $.fn.inViewport = function() {
        return !($(window).height() + $(window).scrollTop() <= this.offset().top - 200) &&
               !($(window).width() + $(window).scrollLeft() <= this.offset().left - 200) &&
               !($(window).scrollTop() >= this.offset().top + 200 + this.height()) &&
               !($(window).scrollLeft() >= this.offset().left + 200 + this.width());
    };
})(jQuery);

(function($){
    $.fn.responsiveBackgrounds = function(reset) {
        this.each(function(){
            if (reset)
                $(this).data('status', 'new');
            if (!$(this).inViewport() || $(this).data('status') === 'done')
                return;
            src = $(this).data('src_desktop');
        <?php foreach ($sizes as $size => $mediaQuery): ?>
        <?php if ($notFirst): ?>else <?php endif; ?>if ($(this).data('src_<?php echo $size; ?>') && window.matchMedia('<?php echo $mediaQuery; ?>').matches)
            src = $(this).data('src_<?php echo $size; ?>');
        <?php $notFirst = true; ?>
        <?php endforeach; ?>

            if ($(this).css('background-image') !== "url('" + src + "')")
                $(this).css('background-image', "url('" + src + "')");

            $(this).data('status', 'done');
        });
    };
})(jQuery);

(function($){
    $.fn.responsiveImages = function(reset) {
        this.each(function(){
            var $self = $(this);
            if (reset)
                $(this).data('status', 'new');
            if (!$(this).inViewport() || $(this).data('status') === 'done')
                return;
            src = $(this).data('src_desktop');
        <?php $notFirst = false; foreach ($sizes as $size => $mediaQuery): ?>
        <?php if ($notFirst): ?>else <?php endif; ?>if ($(this).data('src_<?php echo $size; ?>') && window.matchMedia('<?php echo $mediaQuery; ?>').matches)
            src = $(this).data('src_<?php echo $size; ?>');
        <?php $notFirst = true; ?>
        <?php endforeach; ?>
            if (!$(this).next('.generated-responsive-image').length)
            {
                $(this).after('<img class="generated-responsive-image ' + $(this).data("class") + '" src="' + src + '" />');
                if ($(this).data('style')) {
                    $(this).next('.generated-responsive-image').attr('style', $(this).data('style'));
                }
            } else if ($(this).next('.generated-responsive-image').attr('src') !== src) {
                $(this).next('.generated-responsive-image').attr('src', src);
            }
            $(this).data('status', 'done');
        });
        return this;
    };
})(jQuery);
