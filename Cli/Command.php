<?php

namespace WPC\Cli;

abstract class Command
{

    public $options;
    public $isVerbose = false;

    protected $lockHandler;

    public function __construct($options = null)
    {
        $opts = getopt('', ['verbose', 'no-locking']);
        $this->isVerbose = isset($opts['verbose']);

        if (!isset($opts['no-locking'])) {
            $this->getLockOrExit();
            register_shutdown_function([$this, 'releaseLock']);
        }

        $this->setOptions($options);
        $this->init();
    }

    protected function init()
    {

    }

    abstract public function run($params = null);

    public function setOptions($options)
    {
        if (!is_array($options)) {
            return;
        }

        foreach ($options as $option => $value) {
            $this->setOption($option, $value);
        }
    }

    public function setOption($option, $value)
    {
        if (!property_exists($this, $option)) {
            return;
        }

        $method = 'set' . $option;
        if (method_exists($this, $method)) {
            $this->{$method}($value);
        } else {
            $this->{$option} = $value;
        }
    }

    protected function fail($message, $data = null)
    {
        $backtrace = debug_backtrace(false);
        $caller = $backtrace[1];
        $codeLines = array_slice(file($backtrace[0]['file']), $backtrace[0]['line'] - 6, 11);
        $message = $message . "\n\n-------------------------------\n" .
                "File: " . $backtrace[0]['file'] . "\n" .
                "Osztály: " . $caller['class'] . "\n" .
                "Metódus: " . $caller['function'] . "\n" .
                "Paraméterek: " . join(' ', $caller['args']) . "\n" .
                "Sor: " . $caller['line'] . "\n" .
                "Kód: \n" . join("", $codeLines) . "\n";
        if ($data) {
            $message .= "-------------------------------\n" . print_r($data, true);
        }

        echo($message);
        die();
    }

    protected function verboseLog($message, $flush = true)
    {
        if (!$this->isVerbose) {
            return;
        }

        $now = date('H:i:s');
        echo "[{$now}] {$message}\n";
        if ($flush) {
            flush();
            ob_flush();
        }
    }

    protected function getLockFilename()
    {
        $filenameParts = [
            App()->getParam('namespace'),
            get_called_class(),
            md5(json_encode($this->options)),
            'lock'
        ];

        return sys_get_temp_dir() . '/'. implode('.', $filenameParts);
    }

    public function getLockOrExit()
    {
        $this->lockHandler = fopen($this->getLockFilename(), 'c+');
        if (!flock($this->lockHandler, LOCK_EX | LOCK_NB)) {
            fclose($this->lockHandler);
            return false;
        }

        return true;
    }

    public function releaseLock()
    {
        if ($this->lockHandler && is_resource($this->lockHandler)) {
            flock($this->lockHandler, LOCK_UN);
            fclose($this->lockHandler);
        }
    }
}