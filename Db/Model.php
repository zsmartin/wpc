<?php

namespace WPC\Db;

class Model extends \WPC\Model
{
    protected static $tableName = '';
    protected static $primaryKey = 'id';

    /**
     * Létrehoz egy modelpéldányt egy adatbázis rekord alapján,
     * beállítja az attribútumok értékeit és végigfuttatja rajtuk
     * a dbFilters() metódusban meghatározott módosításokat
     *
     * @param array $data az adatbázisból kiolvasott rekord
     * @return static egy új modelpéldány
     */
    public static function createFromDb($data)
    {
        $object = self::create($data);
        foreach (array_flip($object->attributes()) as $attr => $value) {
            $object->$attr = $object->applyDbFilter($attr, 'read');
        }

        return $object;
    }

    /**
     * Visszaadja a modelhez tartozó adatbázistábla nevét, prefix-szel együtt.
     * Ha nincs megadva a $tableName, akkor alapértelmezetten a model nevét veszi, kisbetűsítve
     *
     * @see $tableName
     * @return string az adatbázistábla neve, prefix-szel
     */
    public static function getTableName()
    {
        $basename = mb_strlen(static::$tableName) ? static::$tableName : strtolower(static::className());
        return App()->getDb()->prefix . $basename;
    }

    /**
     * Visszaadja az osztály rövid nevét (namespace nélkül)
     *
     * @return string az osztály rövid neve
     */
    public static function className()
    {
        $rc = new \ReflectionClass(get_called_class());
        return $rc->getShortName();
    }

    /**
     * Visszaadja a model attribútumait, miután végigfuttatta rajtuk
     * a dbFilters()-ben megadott módosításokat
     *
     * @see applyDbFilter()
     * @return array az adatbázisba kiírandó attribútumok
     */
    protected function getAttributesToSave()
    {
        $attributes = array_flip($this->getAttributes());
        foreach ($attributes as $name => $value)
        {
            if (isset($this->$name)) {
                $attributes[$name] = stripslashes($this->applyDbFilter($name, 'write'));
            } else {
                unset($attributes[$name]);
            }
        }

        return $attributes;
    }

    /**
     * A model attribútumaira alkalmazandó változtatásokat tartalmazza,
     * miután kiolvastuk őket az adatbázisból, illetve mielőtt visszaírjuk
     * őket oda.
     *
     * Példa:
     * ha egy mezőt az adatbázisban integer timestampként tárolunk, de a kódban
     * dátum stringként szeretnénk használni:
     * return array(
     *      'read' => array(
     *          'timestamp' => function($value) {
     *              return date('Y-m-d H:i:s', $value);
     *          }
     *      ),
     *      'write' => array(
     *          'timestamp' => function($value) {
     *              return strtotime($value);
     *          }
     *      )
     * );
     *
     * @return array a módosításokat tartalmazó tömb
     */
    public function dbFilters()
    {
        return array();
    }

    /**
     * Egy attribútum értékére lefuttatja a dbFilters() hozzárendelt tartalmát,
     * attól függően, hogy épp kiolvasás utáni vagy írás előtti állapotban vagyunk.
     *
     * @param string $field az attribútum neve
     * @param string $type read vagy write
     * @return mixed a módosított érték
     */
    protected function applyDbFilter($field, $type = 'write') {
        if (isset($this->dbFilters()[$type][$field])) {
            $callback = $this->dbFilters()[$type][$field];
            return $callback($this->$field);
        }
        return $this->$field;
    }

    /**
     * A modelt új rekordként beszúrja az adatbázisba
     *
     * @uses beforeSave() beszúrás előtt módosíthatunk a modelen
     * @uses afterSave() beszúrás után az eredménytől függően pl. üríthetjük a cachet
     * @return int|boolean a beszúrt rekord id-je vagy false, ha nem sikerült a művelet
     */
    protected function insert()
    {
        $this->beforeSave();
        $attributes = $this->getAttributesToSave();
        $result = App()->getDb()->insert(static::getTableName(), $attributes);
        $this->{static::$primaryKey} = App()->getDb()->insert_id;
        $this->afterSave($result);

        return $result ? $this->{static::$primaryKey} : false;
    }

    /**
     * Módosítja a modelhez tartozó adatbázisrekord tartalmát
     *
     * @uses beforeSave() beszúrás előtt módosíthatunk a modelen
     * @uses afterSave() beszúrás után az eredménytől függően pl. üríthetjük a cachet
     * @return int|boolean a módosított rekord id-je vagy false, ha nem sikerült a művelet
     */
    protected function update()
    {
        $this->beforeSave();
        $attributes = $this->getAttributesToSave();
        $result = App()->getDb()->update(static::getTableName(), $attributes, array(static::$primaryKey => $this->id));
        $this->afterSave($result);

        return $result !== false ? $this->id : false;
    }

    /**
     * Elmenti a modelt az adatbázisba
     *
     * @return int|boolean a modelhez tartozó rekord id-je vagy false, ha nem sikerült a művelet
     */
    public function save()
    {
        if ($this->id) {
            $result = $this->update();
        } else {
            $result = $this->insert();
        }

        return $result;
    }

    /**
     * Adatbázisba való kiírás előtt fut le
     */
    public function beforeSave()
    {
    }

    /**
     * Adatbázisba való kiírás után fut le
     *
     * @param int|boolean $result a modelhez tartozó rekord id-je vagy false, ha nem sikerült a művelet
     */
    public function afterSave($result)
    {
    }

    /**
     * Egy rekordot töröl az adatbázisból, id alapján
     *
     * @param string $id a törlendő rekord id-je
     * @return int|boolean a törölt sorok száma vagy false, ha nem sikerült a művelet
     */
    public static function delete($id)
    {
        return App()->getDb()->delete(static::getTableName(), array(static::$primaryKey => $id));
    }

    /**
     * Elsődleges kulcs alapján kikeres egy rekordot az adatbázisból és létrehoz ez alapján egy modelpéldányt
     *
     * @param string $id az elsődleges kulcs értéke
     * @return static|boolean a megtalált model, vagy false ha nincs ilyen rekord
     */
    public static function findById($id)
    {
        if (!$id) {
            return false;
        }
        $tableName = static::getTableName();
        $pk = static::$primaryKey;
        $sql = App()->getDb()->prepare("SELECT * FROM `{$tableName}` WHERE {$pk} = %s", array($id));
        $model = App()->getDb()->get_row($sql);
        if (!$model) {
            return false;
        }
        return static::createFromDb($model);
    }

    /**
     * A megadott feltételeknek megfelelő adatbázis rekordokból létrehoz modelpéldányokat
     *
     * @param string $where keresési feltétel SQL formában
     * @return static[]|boolean modelekből álló tömb, vagy false ha egy rekord sem felelt meg a feltételnek
     */
    public static function findAll($where = '')
    {
        $tableName = static::getTableName();
        $result = App()->getDb()->get_results("SELECT * FROM `{$tableName}` " . $where);
        if (!$result) {
            return false;
        }
        $models = array();
        foreach ($result as $row) {
            $models[] = static::createFromDb($row);
        }

        return $models;
    }
}