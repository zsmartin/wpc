<?php

namespace WPC\Admin;

abstract class Page
{
    public $validator = array();

    /** @var \WPC\Template */
    public $template;

    /** @var \WPC\Admin\Manager */
    public $manager;

    public function __construct()
    {
        $this->init();
    }

    public function init()
    {
    }

    public abstract function getIndex($data = null);

    public function redirect($action, array $args = null)
    {
        $parts = explode('.', $action);
        if (1 === count($parts))
        {
            $pagename = array_pop(explode('\\', get_class($this)));
            $action = $parts[0];
        }
        else
        {
            $pagename = $parts[0];
            $action = $parts[1];
        }

        $pagename = App()->getAdminManager()->pagenamePrefix . lcfirst($pagename);
        $action = lcfirst($action);

        $queryString = '';
        if (!is_null($args))
            foreach ($args as $name => $value)
                $queryString .= '&' . $name . '=' . $value;

        wp_redirect(admin_url("admin.php?page={$pagename}&action={$action}{$queryString}"));
        die();
    }

    public function validate($data)
    {
        foreach ($this->validator as $var => $validator)
        {
            if (!isset($data[$var]))
            {
                if (isset($validator['default']))
                    $data[$var] = $validator['default'];
            }
            else
            {
                if (!isset($validator['options']))
                    $validator['options'] = array();
                $result = filter_var($data[$var], $validator['type'], array('options' => $validator['options']));
                if (false === $result)
                {
                    if (!isset($validator['default']))
                        unset($data[$var]);
                    else
                        $data[$var] = $validator['default'];
                }
            }
        }

        return $data;
    }

    public function render(array $data = null, $view = null, $display = true)
    {
        if (is_null($view) || false === strpos($view, '.'))
        {
            list(, $caller) = debug_backtrace(false);
            $parts = explode('\\' , $caller['class']);
            array_shift($parts);
            $view = is_null($view) ? substr($caller['function'], 3) : $view;
            $view = join('.', $parts) . '.' . mb_strtolower($view);
        }

        $template = new \WPC\Template($view);
        $template->set($data);
        return $template->render($display);
    }
}