<?php

namespace WPC\Admin;
use \WPC\Component,
    \WPC\Exception,
    \WPC\Admin\Page,
    \WPC\Template;

class Manager extends Component
{
    public $defaults = array(
        'pagename' => 'settings',
        'action' => 'index'
    );

    public $pagenamePrefix = 'wpc-';

    public $namespace;

    public $adminPages = array();

    public function wpAdminInit()
    {
        $this->showFlashes();
        $this->loadPages();

        if ('post' === mb_strtolower($_SERVER['REQUEST_METHOD'])) {
            $handled = $this->route();
            if ($handled) {
                die();
            }
        }
    }

    public function route()
    {
        $type = mb_strtolower($_SERVER['REQUEST_METHOD']);
        if (!in_array($type, array('get', 'post'))) {
            return false;
        }

        $data = 'get' === $type ? $_GET : $_POST;
        if (!isset($data['page']) || substr($data['page'], 0, strlen($this->pagenamePrefix)) !== $this->pagenamePrefix) {
        //if (!isset($data['handler']) || 'wpc' !== $data['handler'])
            return false;
        }

        if (!isset($data['page'])) {
            $data['page'] = $this->defaults['pagename'];
        }
        else {
            $data['page'] = substr($data['page'], strlen($this->pagenamePrefix));
        }

        $classname = $this->namespace . '\\Admin\\Pages\\' . ucfirst($data['page']);
        if (class_exists($classname) && is_subclass_of($classname, '\\WPC\\Admin\\Page')) {
            $page = new $classname();

            if (!isset($data['action'])) {
                $pageMeta = $classname::loadMeta();
                $data['action'] = isset($pageMeta['defaultAction']) ? $pageMeta['defaultAction'] : $this->defaults['action'];
            }

            $actionName = join('', array_map(function($part) { return ucfirst($part); }, explode('-', $data['action'])));
            $methodName = $type . $actionName;

            if (method_exists($page, $methodName)) {
                try {
                    if ('get' === $type) {
                        $page->template = new Template('Admin.Pages.' . ucfirst($data['page']) . '.' . lcfirst($actionName));
                    }
                    $page->manager = $this;
                    $page->$methodName($data);
                } catch (Exception $e) {
                    $this->addFlash($e->getMessage(), 'error');
                    if ('get' === $type) {
                        $page->redirect($actionName);
                    } else {
                        $page->redirect($this->defaults['action']);
                    }
                }
                return true;
            }
        }

        return false;
    }

    public function addFlash($message, $level = 'info')
    {
        if ('' === session_id()) {
            session_start();
        }

        if (!isset($_SESSION['flash_messages'])) {
            $_SESSION['flash_messages'] = array();
        }

        $_SESSION['flash_messages'][] = array('content' => $message, 'level' => $level);

        return count($_SESSION['flash_messages']) - 1;
    }

    public function removeFlash($idOrMessage)
    {
        if ('' === session_id()) {
            session_start();
        }

        if (is_int($idOrMessage) && isset($_SESSION['flash_messages'][$id])) {
            unset($_SESSION['flash_messages'][$id]);
        } else {
            foreach ($_SESSION['flash_messages'] as $id => $message) {
                if ($message->content === $idOrMessage) {
                    unset($_SESSION['flash_messages'][$id]);
                }
            }
        }
    }

    public function removeFlashes()
    {
        if ('' === session_id()) {
            session_start();
        }

        $_SESSION['flash_messages'] = array();
    }

    public function showFlashes()
    {
        add_action('admin_notices', function(){
            if ('' === session_id()) {
                session_start();
            }

            $template = new Template('Partials.flashmessage');
            if (!empty($_SESSION['flash_messages'])) {
                foreach ($_SESSION['flash_messages'] as $id => $message) {
                    $template->set('class', ('error' == $message['level']) ? 'error' : 'updated');
                    $template->set('message', $message['content']);
                    $template->render(true);

                    unset($_SESSION['flash_messages'][$id]);
                }
            }
        });
    }

    public function loadPages()
    {
        $cache = App()->getCache();
        if (empty($this->adminPages) && false === ($this->adminPages = $cache->get('admin-pages'))) {
            $pageFiles = APPLICATION_PATH . '/components/Admin/Pages/*.php';

            $this->adminPages = array();
            foreach (glob($pageFiles) as $page) {
                $pathParts = explode('/', $page);
                $filename = array_pop($pathParts);
                $className = $this->namespace . '\\Admin\\Pages\\' . substr($filename, 0, -4);
                try {
                    $pageObject = new $className();
                    if ($pageObject instanceof Page) {
                        $pageMeta = $className::loadMeta();
                        $this->adminPages[] = array(
                            'title' => $pageMeta['title'],
                            'slug' => lcfirst(substr($filename, 0, -4)),
                            'function' => array($this, 'route'), //isset($pageMeta['defaultAction']) ? array($pageObject, $pageMeta['defaultAction']) : array($pageObject, 'getIndex'),
                            'weight' => isset($pageMeta['weight']) ? $pageMeta['weight'] : 10
                        );
                    }
                } catch (Exception $e) {
                    $this->addFlash($e->getMessage, 'error');
                }
            }

            if (!empty($this->adminPages)) {
                usort($this->adminPages, function($a, $b){ return ($a['weight'] < $b['weight']) ? 1 : ($a['weight'] > $b['weight']) ? -1 : 0;});
                $cache->set('admin-pages', $this->adminPages, 300);
            }
        }

        if (!empty($this->adminPages)) {
            $self = $this;
            add_action('admin_menu', function() use ($self) {
                add_menu_page(get_bloginfo('title'), get_bloginfo('title'), 'manage_options', $self->pagenamePrefix . $self->adminPages[0]['slug'], $self->adminPages[0]['function']);
                foreach ($self->adminPages as $adminPage) {
                    add_submenu_page($self->pagenamePrefix . $self->adminPages[0]['slug'], $adminPage['title'], $adminPage['title'], 'manage_options', $self->pagenamePrefix . $adminPage['slug'], $adminPage['function']);
                }
            });
        }
    }
}