<?php

namespace WPC;

use \WPC\Component;

class WP extends Component
{
    public $config;

    public function init()
    {
        $this->config = App()->getConfig('wp');

        add_action('init', function(){
            $this->registerTaxonomies($this->config->get('taxonomy'));
            $this->registerPostTypes($this->config->get('post_type'));
            $this->registerImageSizes($this->config->get('image_size'));
            $this->setupAdminBar();
        });

        add_action('wp_enqueue_scripts', function(){
            $this->registerClientScripts(App()->getConfig('clientScripts')->get());
            //$this->registerClientScript('responsive-js', 'js', \WPC\ResponsiveImage::getResponsiveJs(), 'footer');
        });

        $translations = $this->config->get('translate', 'default');
        if ($translations === 'admin') {
            $this->onlyAdminTextDomains();
        } else if ($translations === 'cache') {
            $this->cacheTextDomains();
        }

        require_once(dirname(__FILE__) . '/WpPlugs.php');
    }

    public function setupAdminBar()
    {
        if ($this->config->get('show_admin_bar') === false) {
            add_filter('show_admin_bar', function () {
                return false;
            });
        }
    }

    public function registerTaxonomies($taxonomies)
    {
        if (is_array($taxonomies)) {
            foreach ($taxonomies as $name => $taxonomy) {
                register_taxonomy($name, $taxonomy['type'], $taxonomy['args']);
            }
        }
    }

    public function registerPostTypes($postTypes)
    {
        if (is_array($postTypes)) {
            foreach ($postTypes as $name => $postType) {
                register_post_type($name, $postType);
            }
        }
    }

    public function registerImageSizes($imageSizes) {
        if (!is_array($imageSizes)) {
            return;
        }

        \WPC\ResponsiveImage::useDefaultBreakpoints();
        add_theme_support('post-thumbnails');

        foreach ($imageSizes as $imageSize => $dimensions) {
            $index = 0;
            foreach (\WPC\ResponsiveImage::$sizes as $respSize => $mediaQuery) {
                if (strpos($respSize, 'x2') === strlen($respSize) - 2) {
                    continue;
                }

                if (isset($dimensions[$index])) {
                    list($width, $height) = explode('x', $dimensions[$index]);
                    if (strpos($width, '!') === 0) {
                        $crop = false;
                        $width = substr($width, 1);
                    } else {
                        $crop = true;
                    }
                }

                add_image_size($imageSize . '-' . $respSize, $width, $height, $crop);
                add_image_size($imageSize . '-' . $respSize . 'x2', $width * 2, $height * 2, $crop);

                $index++;
            }
        }

        App()->getUtils()->addJsVar('imageSizes', \WPC\ResponsiveImage::$sizes);
    }

    public function registerClientScript($name, $type, $uri='', $position = 'header', $condition = true)
    {
        if ($condition instanceof \Closure) {
            $condition = $condition();
        }

        if ($condition !== true) {
            return;
        }

        if ($position === 'deregister') {
            if ($type === 'js') {
                wp_deregister_script($name);
            } else {
                wp_deregister_style($name);
            }
            return;
        }

        if (strpos($uri, '%') === 0) {
            $src = preg_replace('@%@', get_template_directory(), $uri);
            $uri = preg_replace('@%@', get_template_directory_uri(), $uri);
            $version = filemtime($src);
        } else {
            $src = '';
            $version = false;
        }

        if ($type === 'js') {
            wp_enqueue_script($name, $uri, array(), $version, $position === 'footer');
        } else {
            wp_enqueue_style($name, $uri, array(), $version);
        }
    }

    public function registerClientScripts($csConfig)
    {
        foreach ($csConfig as $type => $positions) {
            if (!in_array($type, array('js', 'css'))) {
                throw new Exception('Érvénytelen ClientScript típus: ' . $type);
            }

            foreach ($positions as $position => $scripts) {
                if (!in_array($position, array('header', 'footer', 'deregister'))) {
                    throw new Exception('Érvénytelen ClientScript pozíció: ' . $position);
                }

                foreach ($scripts as $name => $uri) {
                    if (is_array($uri)) {
                        $condition = $uri['condition'];
                        $uri = $uri['src'];
                    } else {
                        $condition = true;
                        if (is_int($name)) {
                            $name = $uri;
                            $uri = '';
                        }
                    }
                    $this->registerClientScript($name, $type, $uri, $position, $condition);
                }
            }
        }
    }

    public function onlyAdminTextDomains()
    {
        add_filter('override_load_textdomain', function($override, $domain, $mofile) {
            global $l10n;
            if (!is_admin() || defined('DOING_AJAX') && DOING_AJAX) {
                $l10n[$domain] = new \NOOP_Translations();
                return true;
            }
            return false;
        }, 1, 3);
    }

    public function cacheTextDomains()
    {
        add_filter('override_load_textdomain', function($override, $domain, $mofile) {
            global $l10n;
            $cache = App()->getCache();
            $key = 'load_textdomain-' . $domain;

            $cachedDomain = $cache->get($key);
            if (false === $cachedDomain) {
                do_action('load_textdomain', $domain, $mofile);
                $mofile = apply_filters('load_textdomain_mofile', $mofile, $domain);
                $mo = new \MO();
                if (!is_readable($mofile) || !$mo->import_from_file($mofile)) {
                    return false;
                }

                $data = array(
                    'entries' => $mo->entries,
                    'headers' => $mo->headers
                );
                $cache->set($key, $data, 3600, MEMCACHE_COMPRESSED);
            } else {
                $mo = new \MO();
                $data = $cachedDomain;
                $mo->entries = $data['entries'];
                $mo->set_headers($data['headers']);
            }

            if (isset($l10n[$domain])) {
                $mo->merge_with($l10n[$domain]);
            }

            $l10n[$domain] = &$mo;

            return true;
        }, 1, 3);
    }
}