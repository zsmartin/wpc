<?php

namespace WPC;

class Paginator
{
    public $currentPage;
    public $totalPages;
    public $baseUrl;

    public $ajaxAction = '';
    public $ajaxFields = array();

    public $template;

    public function __construct($templateName = 'paginator')
    {
        $this->setTemplate($templateName);
    }

    public function setTemplate($templateName)
    {
        if (strpos($templateName, '.') === false)
                $this->template = 'Partials.' . $templateName;
        else
            $this->template = $templateName;

        return $this;
    }

    public function isFirstPage()
    {
        return $this->currentPage === 1;
    }

    public function isLastPage()
    {
        return $this->currentPage === $this->totalPages;
    }

    public function render($display = false)
    {
        $this->validate();

        if (1 === $this->totalPages)
            return;

        $template = new Template($this->template);
        $template->set('paginator', $this);
        $template->render($display);
    }

    public function validate()
    {
        if (!$this->currentPage)
            $this->currentPage = (int)get_query_var('paged');

        if ($this->currentPage < 1)
            $this->currentPage = 1;

        if (!$this->totalPages)
            $this->totalPages = (int)$GLOBALS['wp_query']->max_num_pages;

        if ($this->totalPages < 1)
            $this->totalPages = 1;

        if (!$this->baseUrl)
            $this->baseUrl = get_permalink();
    }
}
