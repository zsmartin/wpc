<?php

namespace WPC;

class Config
{
    protected $data = array();

    public function __construct($namespace = '')
    {
        $this->data = App()->config;
        if (strlen($namespace)) {
            $this->data = $this->get($namespace);
        }
    }

    public function get($key = null, $default = null)
    {
        if (is_null($key)) {
            return $this->data;
        }

        $config = $this->data;
        $parts = explode('.', $key);
        foreach ($parts as $part) {
            if (isset($config[$part])) {
                if (is_array($config[$part])) {
                    $config = $config[$part];
                } elseif ($config[$part] instanceof \Closure) {
                    return $config[$part]();
                } else {
                    return $config[$part];
                }
            } else {
                return $default;
            }
        }

        return $config;
    }

    public function set($key, $value)
    {
        $config = &$this->data;
        $parts = explode('.', $key);
        foreach ($parts as $index => $part) {
            if (isset($config[$part])) {
                if (is_array($config[$part])) {
                    $config = $config[$part];
                } elseif ($index < (count($parts) - 1)) {
                    throw new Exception('Invalid config parent key ' . $part . ' in ' . $key);
                }
            } else {
                if ($index < (count($parts) - 1)) {
                    $config[$part] = array();
                } else {
                    $config[$part] = $value;
                }
            }
        }
    }
}
