<?php

namespace WPC;

class ResponsiveImage
{
    public static $sizes = array();

    public $images = array();
    public $alt;

    public static function registerSize($name, $mediaQuery)
    {
        self::$sizes[$name] = $mediaQuery;
        self::$sizes[$name . 'x2'] = $mediaQuery . ' and (min-resolution: 144dpi)';
    }

    public function setImage($size, $image)
    {
        $this->images[$size] = $image;
    }

    public function setImagesByThumbnail($postId, $size)
    {
        $thumbnailId = get_post_thumbnail_id($postId);
        if (!$thumbnailId)
        {
            $this->images = array();
            return;
        }

        $this->setImagesByAttachment($thumbnailId, $size);
    }

    public function setImagesByAttachment($attachmentId, $size)
    {
        $this->images = array();

        $this->alt = trim(strip_tags(get_post_meta($attachmentId, '_wp_attachment_image_alt', true)));

        /* @var $cache Caches\Memcache */
        $cache = App()->getCache();
        $key = md5(json_encode(array($attachmentId, $size)));
        if (false !== ($images = $cache->get('responsive-image-' . $key)))
        {
            foreach ($images as $size => $thumb)
                $this->images[$size] = $thumb;
        }
        else
        {
            $cachedImages = array();
            foreach (array_keys(self::$sizes) as $respSize)
            {
                $thumb = wp_get_attachment_image_src($attachmentId, $size . '-' . $respSize);
                if ($thumb)
                {
                    $this->images[$respSize] = $thumb[0];
                    $cachedImages[$respSize] = $thumb[0];
                }
            }
            $cache->set('responsive-image-' . $key, $cachedImages, 0);
        }
    }

    public function getBackgroundAttrs()
    {
        if (empty($this->images))
            return '';

        $attrs = array('data-type="responsive-background"');

        foreach ($this->images as $size => $image)
        {
            $attrs[] = 'data-src_' . $size . '="' . $image . '"';
        }
        $attrs = join(' ', $attrs);

        return ' ' . $attrs . ' ';
    }

    public function getImgTag($display = false, $attributes = array())
    {
        if (!$this->images)
            return '';

        $attrs = array();
        $dataAttrs = array();

        $srcAttrs = array();
        foreach ($this->images as $size => $image)
            $srcAttrs[] = 'data-src_' . $size . '="' . $image . '"';

        if ($attributes)
        {
            foreach ($attributes as $attr => $value)
            {
                $attrStr = $attr . '="' . $value . '"';
                $attrs[] = $attrStr;
                $dataAttrs[] = 'data-' . $attrStr;
            }
        }
        else
            $attrs = array();

        $images = array_values($this->images);
        $template = new Template('Partials.responsive-images');
        $template->set(array(
            'mobileThumb' => array_shift($images),
            'alt' => $this->alt,
            'srcAttrs' => ' ' . join(' ', $srcAttrs) . ' ',
            'attrs' => ' ' . join(' ', $attrs) . ' ',
            'dataAttrs' => ' ' . join(' ', $dataAttrs) . ' '
        ));

        return $template->render($display);
    }

    public static function getResponsiveJs()
    {
        $hash = substr(md5(json_encode(self::$sizes)), 0, 10);
        $jsName = "responsive-images.{$hash}.js";
        $uploadDir = wp_upload_dir();
        if (is_readable($uploadDir['basedir'] . '/js/' . $jsName))
            return $uploadDir['baseurl'] . '/js/' . $jsName;

        $template = new Template('Js.responsive-images');
        $template->set('sizes', self::$sizes);
        $jsFileContents = $template->render();

        file_put_contents($uploadDir['basedir'] . '/js/' . $jsName, $jsFileContents);
        chmod($uploadDir['basedir'] . '/js/' . $jsName, 0664);

        return $uploadDir['baseurl'] . '/js/' . $jsName;
    }

    public static function useDefaultBreakpoints()
    {
        ResponsiveImage::registerSize('320', 'screen and (max-width:320px)');
        ResponsiveImage::registerSize('480', 'screen and (max-width:480px)');
        ResponsiveImage::registerSize('768', 'screen and (max-width:768px)');
        ResponsiveImage::registerSize('1024', 'screen and (max-width:1024px)');
        ResponsiveImage::registerSize('desktop', 'screen and (min-width:1025px)');
    }
}
