<?php

namespace WPC;

class Api extends Component
{
    public function init()
    {
        $this->registerRoute();
        $this->processApiRoutes();
    }

    public function registerRoute()
    {
        add_filter('rewrite_rules_array', function($rules) {
            $newRules = array(
                'api/([^/]+)/([0-9]+)/?' => 'index.php?wpc_route=api&apicontroller=$matches[1]&apiid=$matches[2]',
                'api/([^/]+)/?' => 'index.php?wpc_route=api&apicontroller=$matches[1]',
            );

            return $newRules + $rules;
        });

        add_filter('query_vars', function($vars) {
            $vars[] = 'wpc_route';
            $vars[] = 'apicontroller';
            $vars[] = 'apiid';
            return $vars;
        });
    }

    public function processApiRoutes()
    {
        add_action('parse_request', function(\WP $wp) {
            if (!isset($wp->query_vars['wpc_route']) ||
                $wp->query_vars['wpc_route'] !== 'api' ||
                !isset($wp->query_vars['apicontroller'])) {
                return;
            }

            $route = $wp->query_vars['apicontroller'];
            $className = App()->getParam('namespace') . '\\Api\\' . ucfirst($route);
            if (!class_exists(App()->getParam('namespace') . '\\Api\\' . ucfirst($route))) {
                $className = '\\WPC\\Api\\' . ucfirst($route);
            }

            if (!class_exists($className)) {
                header(':', true, 404);
                header('X-Class:'.$className);
                die();
            }

            $controller = new $className();
            $controller->resourceId = isset($wp->query_vars['apiid']) ? $wp->query_vars['apiid'] : null;
            $requestMethod = strtolower($_SERVER['REQUEST_METHOD']);
            if ($requestMethod === 'head') {
                $requestMethod = 'get';
                $controller->sendBody = false;
            }

            if (!$controller->resourceId) {
                if ($requestMethod === 'get') {
                    $action = 'index';
                    $data = $_GET;
                } elseif ($requestMethod === 'post') {
                    $action = 'create';
                    $data = $_POST;
                } else {
                    $action = '_unsupported_';
                }
            } else {
                if ($requestMethod === 'get') {
                    $action = 'view';
                    $data = $_GET;
                } elseif ($requestMethod === 'put') {
                    $action = 'update';
                    $data = $_POST;
                } elseif ($requestMethod === 'delete') {
                    $action = 'delete';
                    $data = $_POST;
                } else {
                    $action = '_unsupported_';
                }
            }

            if (isset($data['format'])) {
                $controller->format = $data['format'];
                unset($data['format']);
            }

            $controller->setResourceFilters($data);

            $controller->runAction($action);
            die();
        });
    }
}