<?php

namespace WPC\Facebook;

use \WPC\Component;

class GraphMeta extends Component
{
    public $app_id;
    public $site_name;
    public $locale;
    public $title;
    public $url;
    public $image;
    public $type;
    public $description;

    protected $defaultShareImage;
    protected static $metaFields = array(
        'app_id', 'site_name', 'locale', 'title', 'url', 'image', 'type', 'description'
    );

    public function init()
    {
        $self = $this;
        add_action('wp_head', function() use ($self) {
            echo $self->toHTML();
        });
    }

    public function getAppId()
    {
        return $this->app_id;
    }

    public function getSiteName()
    {
        if (isset($this->site_name)) {
            return $this->site_name;
        } else {
            return get_option('blogname');
        }
    }

    public function getLocale()
    {
        if (isset($this->locale)) {
            return $this->locale;
        } else {
            $appLang = App()->getParam('lang');
            if ($appLang) {
                return $appLang;
            } else {
                return 'hu_HU';
            }
        }
    }

    public function getTitle()
    {
        if (isset($this->title)) {
            return $this->title;
        } elseif (is_single()) {
            return get_the_title();
        } elseif (is_category()) {
            return single_cat_title('', false);
        } else {
            return get_option('blogname');
        }
    }

    public function getUrl()
    {
        if (isset($this->url)) {
            return $this->url;
        } else {
            return site_url($GLOBALS['wp']->request . '/');
        }
    }

    public function getImage()
    {
        if (isset($this->image)) {
            return $this->image;
        } elseif (is_single()) {
            $thumbnail = wp_get_attachment_image_src(get_post_thumbnail_id(), 'original');
            if ($thumbnail) {
                return $thumbnail[0];
            }
        }

        if (is_readable($this->getDefaultShareImage())) {
            return $this->getDefaultShareImage();
        } else {
            return false;
        }
    }

    public function getType()
    {
        if (isset($this->type)) {
            return $this->type;
        } elseif (is_single()) {
            return 'article';
        } else {
            return 'website';
        }
    }

    public function getDescription()
    {
        if (isset($this->description)) {
            return $this->description;
        } elseif (is_single()) {
            return get_the_excerpt();
        } else if (is_category()) {
            return ($desc = trim(strip_tags(category_description()))) ? $desc : get_bloginfo('description');
        } else {
            return get_bloginfo('description');
        }
    }

    public function toHTML()
    {
        $result = '';
        $template = '<meta property="%s:%s" content="%s" />' . "\n";
        foreach (self::$metaFields as $fieldName) {
            $parts = array_map('ucfirst', explode('_', $fieldName));
            $methodName = 'get' . implode('', $parts);
            $values = $this->$methodName();
            $prefix = $fieldName === 'app_id' ? 'fb' : 'og';
            if (!is_array($values)) {
                $values = array($values);
            }
            foreach ($values as $value) {
                $result .= sprintf($template, $prefix, $fieldName, esc_attr(strip_tags($value)));
            }
        }

        return $result;
    }

    public function getDefaultShareImage()
    {
        if (!$this->defaultShareImage) {
            $this->defaultShareImage = get_template_directory_uri() . '/img/share.jpg';
        }

        return $this->defaultShareImage;
    }

    public function setDefaultShareImage($image)
    {
        $this->defaultShareImage = $image;
    }
}