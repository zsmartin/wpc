<?php

namespace WPC\Facebook;

use \WPC\Component;

class SDK extends Component
{
    public $appId;
    public $secret;

    protected $facebook;

    public function getConnector()
    {
        if (!$this->facebook) {
            require_once(LIBRARY_PATH . '/Facebook/facebook.php');
            $this->facebook = new \Facebook(array(
                'appId'  => $this->appId,
                'secret' => $this->secret,
            ));
        }

        return $this->facebook;
    }

    public function addToSite()
    {
        add_action('wp_footer', function(){
            if (!$this->appId) {
                return;
            }

            $template = new \WPC\Template('Partials.facebook_sdk');
            $template->set('appId', $this->appId);
            $template->render(true);
        }, 100);
    }

    public function authenticate()
    {
        if (!$this->appId) {
            return false;
        }

        try {
            $user = $this->getConnector()->getUser();
            if (!$user) {
                return false;
            }
            $wpUser = get_user_by('login', $user);
            if (!$wpUser) {
                $userinfo = $this->getConnector()->api('/me');
                $userId = $this->createFbUser($userinfo);
                $wpUser = get_user_by('id', $userId);
            }

            wp_set_current_user($wpUser->ID);
            wp_set_auth_cookie($wpUser->ID, false);
            do_action('wp_login', $wpUser->user_login, $wpUser);

            return true;
        } catch (\FacebookApiException $e) {
              error_log($e->getMessage());
              return false;
        }

        return false;
    }

    public function createFbUser($fbUserInfo)
    {
        $userdata = array(
            'user_pass' => wp_generate_password(32),
            'user_login' => $fbUserInfo['id'],
            'user_email' => $fbUserInfo['email'],
            'display_name' => $fbUserInfo['name'],
            'user_url' => 'facebook'
        );

        $userId = wp_insert_user($userdata);
        if (is_wp_error($userId)) {
            throw new \FacebookApiException(array('error_description' => 'Nem sikerült a facebook usert elmenteni: ' . $userId->get_error_message() . " [{$fbUserInfo['email']}]"));
        }

        return $userId;
    }
}