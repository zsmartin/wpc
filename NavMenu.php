<?php

namespace WPC;

class NavMenu
{
    const CACHEBASE = 'navmenu-';

    public $id;
    public $args;
    public $contents;

    public function __construct(array $args = array())
    {
        $this->args = $args;
    }

    public function getId()
    {
        if ($this->id)
            return $this->id;

        if (isset($this->args['menu']))
        {
            $menu = wp_get_nav_menu_object($this->args['menu']);
            if ($menu)
                return $this->id = $menu->term_id;
        }

        if (isset($this->args['theme_location']))
        {
            $locations = get_nav_menu_locations();
            if (isset($locations[$this->args['theme_location']]))
                return $this->id = $locations[$this->args['theme_location']];
        }

        return false;
    }

    public function getContents()
    {
        if ($this->contents ||
            $this->contents = App()->getCache()->get(self::CACHEBASE . $this->getId()))
        {
            return $this->contents;
        }

        $this->contents = wp_nav_menu(array_merge($this->args, array('echo' => false)));

        if ($this->getId())
            App()->getCache()->set(self::CACHEBASE . $this->getId(), $this->contents);

        return $this->contents;
    }

    public function render()
    {
        echo $this->getContents();
    }

    public static function setupCacheRules()
    {
        add_action('wp_update_nav_menu', function($id){
            App()->getCache()->delete(\WPC\NavMenu::CACHEBASE . $id);
        });
    }
}