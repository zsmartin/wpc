<?php

namespace WPC;

use Exception;

class Model
{
    protected $errors = array();
    protected $scenario = 'default';

    public function __construct($scenario = 'default')
    {
        $this->scenario = $scenario;
        $this->init();
    }

    public function init()
    {
    }

    public function load($data)
    {
        return $this->setAttributes($data);
    }

    public function getScenario()
    {
        return $this->scenario;
    }

    public function setScenario($scenario)
    {
        $this->scenario = $scenario;
    }

    /**
     *
     * @param array $data
     * @return static
     */
    public static function create($data)
    {
        $model = new static;
        $model->setAttributes($data, false);

        return $model;
    }

    public function attributes()
    {
        $class = new \ReflectionClass($this);
        $names = [];
        foreach ($class->getProperties(\ReflectionProperty::IS_PUBLIC) as $property) {
            if (!$property->isStatic()) {
                $names[] = $property->getName();
            }
        }

        return $names;
    }

    public function safeAttributes()
    {
        return array(
            'default' => $this->attributes()
        );
    }

    public function getAttributes($safeOnly = true)
    {
        return $safeOnly ? $this->safeAttributes()[$this->scenario] : $this->attributes();
    }

    public function setAttribute($attr, $value)
    {
        if (!in_array($attr, $this->attributes())) {
            throw new Exception("Unknown attribute: " . $attr);
        }

        $attrParts = explode('_', $attr);
        array_walk($attrParts, function(&$element) {
            $element = ucfirst($element);
        });
        $methodName = 'set' . join('', $attrParts);
        if (method_exists($this, $methodName)) {
            $this->$methodName($value);
        } else {
            $this->$attr = $value;
        }
    }

    public function setAttributes($data, $safeOnly = true)
    {
        if (!is_array($data) && !is_object($data)) {
            throw new Exception('Invalid data source: array or object required');
        }

        $attributes = $safeOnly ? $this->safeAttributes()[$this->scenario] : $this->attributes();
        foreach ($data as $field => $value) {
            if (!in_array($field, $attributes)) {
                continue;
            }

            $this->setAttribute($field, $value);
        }
    }

    public function rules()
    {
        return array(
            '*' => array(
                array('required', array('limit', 'offset', 'type')),
                array(array('int', array('min_range' => 1)), array('limit', 'offset'))
            ),
            'default' => array(

            ),
            'search' => array(

            )
        );
    }

    public function getActiveRules()
    {
        $allRules = $this->rules();
        $activeRules = array();
        if (isset($allRules['*'])) {
            $activeRules = $allRules['*'];
        }
        if (isset($allRules[$this->scenario])) {
            $activeRules = array_merge($activeRules, $allRules[$this->scenario]);
        }

        return $activeRules;
    }

    public function isValid()
    {
        $result = true;
        $this->errors = array();
        foreach ($this->getActiveRules() as $rule) {
            if (!is_array($rule)) {
                throw new Exception("Invalid model rule: " . var_export($rule));
            }

            if (!is_array($rule[0])) {
                $rule[0] = array($rule[0], array());
            } else if (!isset($rule[0][1])) {
                $rule[0][1] = array();
            }
            $validatorMethod = lcfirst($rule[0][0]) . 'Validator';
            $args = $rule[0][1];

            if (!is_array($rule[1])){
                $rule[1] = array($rule[1]);
            }
            foreach ($rule[1] as $field) {
                if (false === $this->$validatorMethod($this->$field, $args)) {
                    $this->errors[$field][] = isset($rule[2]) ? $rule[2] : "Filter '{$rule[0][0]}' failed";
                    $result = false;
                }
            }
        }

        return $result;
    }

    public function fail()
    {
        throw new Exception(json_encode($this->errors), 400);
    }

    /*-------------------------------------------------------*/

    public function requiredValidator($field)
    {
        if (is_null($field)) {
            return false;
        }
        return true;
    }

    public function intValidator($field, $args = array())
    {
        if (false === filter_var($field, FILTER_VALIDATE_INT, array('options' => $args))) {
            return false;
        }
        return true;
    }

    public function urlValidator($field, $args = array())
    {
        if (false === filter_var($field, FILTER_VALIDATE_URL, array('options' => $args))) {
            return false;
        }
        return true;
    }

}