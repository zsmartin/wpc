<?php

namespace WPC\Caches;
use \WPC\Component;

class Dummy extends Component
{
    public function get($key)
    {
        return false;
    }

    public function set($key, $value, $expire = 0)
    {
        return true;
    }

    public function delete($key)
    {
        return true;
    }

    public function flush()
    {
        return true;
    }
}
