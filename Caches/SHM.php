<?php

namespace WPC\Caches;
use \WPC\Component;

class SHM extends Component
{
    public $memorySize = 1971520;
    
    protected $handler;
    protected $lock;
    
    public function init()
    {        
        $this->handler = ftok(__FILE__, 'W');
    }
    
    public static function processKey($key)
    {
        $prefix = App()->getConfig('wp.site_url')->get();
        return $prefix . '.' . APPLICATION_PATH . '.' . $key;
    }
    
    public function get($key = null)
    {
        $id = shmop_open($this->handler, 'c', 0600, $this->memorySize);
        if ($id === false) {
            return false;
        }
        
        $result = shmop_read($id, 0, shmop_size($id));
        $result = unserialize($result);
        shmop_close($id);
        
        if (is_null($key)) {
            return $result;
        }
        
        $key = self::processKey($key);
        if (!isset($result[$key])) {
            return false;
        }
        
        if ($result[$key]['expire'] > 0 && $result[$key]['expire'] < time()) {
            return false;
        }
        
        return $result[$key]['data'];
    }
    
    public function set($key, $value, $expire = 0)
    {
        $item = array(
                'expire' => $expire === 0 ? 0 : time() + $expire,
                'data' => $value,
        );

        $this->lock = sem_get($this->handler, 1, 0600, 1);
        sem_acquire($this->lock);
        
        $shm = $this->get();
        if (!is_array($shm)) {
            $shm = array();
        }

        $key = self::processKey($key);
        $shm[$key] = $item;
        $shm = serialize($shm);
        
        return $this->write($shm, $this->lock);
    }
    
    public function delete($key)
    {
        $this->lock = sem_get($this->handler, 1, 0600, 1);
        sem_acquire($this->lock);
        
        self::processKey($key);
        $shm = $this->get();
        if (isset($shm[$key])) {
            unset($shm[$key]);
        } else if (!is_array($shm)) {
            $shm = array();
        }
        $shm = serialize($shm);
        
        return $this->write($shm);
    }
    
    public function flush()
    {
        $this->lock = sem_get($this->handler, 1, 0600, 1);
        sem_acquire($this->lock);
        
        return $this->write(serialize(array()));
    }
    
    protected function write($shm)
    {
        $id  = shmop_open($this->handler, 'c', 0600, $this->memorySize);
        if ($id) {
            $result = shmop_write($id, $shm, 0) === strlen($shm);
            shmop_close($id);
            sem_release($this->lock);
            return $result;
        } else {
            sem_release($this->lock);
            return false;
        }
    }
}