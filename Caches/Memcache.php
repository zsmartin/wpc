<?php

namespace WPC\Caches;
use \WPC\Component;

class Memcache extends Component
{
    protected $_memcache;
    protected $connected;

    public $host = 'localhost';
    public $port = 11211;

    public function  __destruct()
    {
        $this->getHandler()->close();
    }

    public function get($key)
    {
        $key = self::processKey($key);
        return $this->getHandler()->get($key);
    }

    public function set($key, $value, $expire = 0, $flags = 0)
    {
        $key = self::processKey($key);
        $this->getHandler()->set($key, $value, $flags, $expire);
    }

    public function delete($key)
    {
        $key = self::processKey($key);
        return $this->getHandler()->delete($key);
    }

    public function flush()
    {
        return $this->getHandler()->flush();
    }

    public static function processKey($key)
    {
        $prefix = App()->getConfig('wp.site_url')->get();
        return $prefix . '.' . APPLICATION_PATH . '.' . $key;
    }

    public function getHandler()
    {
        if (!$this->_memcache || !$this->connected)
        {
            $this->_memcache = new \Memcache();
            $this->connected = $this->_memcache->connect($this->host, $this->port);
        }

        if (!$this->connected) {
            return new Dummy();
        }

        return $this->_memcache;
    }

    public function isConnected()
    {
        return $this->connected;
    }
}
