<?php

namespace WPC;

class QueryFilter
{
    public $limit;
    public $orderby;
    public $where;
    public $queriedFields;

    public function __construct()
    {
        $this->clear();
    }

    public function setLimit($count = null, $offset = null)
    {
        if (is_null($count))
            $this->limit = '';
        else
        {
            if (false === filter_var($count, FILTER_VALIDATE_INT, array('options' => array('min_range' => 1))) ||
                ($offset && false === filter_var($offset, FILTER_VALIDATE_INT, array('options' => array('min_range' => 0)))))
                    throw new Exception('Érvénytelen limit filter: ' . $count . ',' . $offset);

            $this->limit = $offset ? " LIMIT {$count} OFFSET {$offset}" : " LIMIT {$count}";
        }

	return $this;
    }

    public function getLimit()
    {
        return esc_sql($this->limit);
    }

    public function setOrderBy($field = null, $direction = 'ASC')
    {
        $direction = mb_strtoupper($direction);
        if (!in_array($direction, array('ASC', 'DESC')))
            throw new Exception('Érvénytelen orderby filter: ' . $direction);

        if (is_null($field))
            $this->orderby = '';
        else
            $this->orderby = " ORDER BY {$field} {$direction}";
    }

    public function getOrderBy()
    {
        return esc_sql($this->orderby);
    }

    protected function addWhere($query, $data = array(), $mode = 'AND')
    {
        global $wpdb;
        if (strlen($this->where))
            $query = ' ' . $mode . ' ' . $query;
        else
            $query = ' WHERE ' . $query;

        $this->where .= $wpdb->prepare($query, $data);
    }

    public function andWhere($query, $data = array())
    {
        $this->addWhere($query, $data, 'AND');
        return $this;
    }

    public function orWhere($query, $data)
    {
        $this->addWhere($query, $data, 'OR');
        return $this;
    }

    public function setWhere($query, $data)
    {
        $this->where = '';
        return $this->andWhere($query, $data);
    }

    public function getWhere()
    {
        return $this->where;
    }

    public function addQueriedFields($fields)
    {
        $this->queriedFields = strlen($this->queriedFields) ? $this->queriedFields . ', ' . $fields : $fields;
    }

    public function setQueriedFields($fields)
    {
        $this->queriedFields = $fields;
    }

    public function queryCount()
    {
        $this->queriedFields = 'COUNT(*) AS count';
    }

    public function getQueriedFields()
    {
        return esc_sql($this->queriedFields);
    }

    public function clear()
    {
        $this->limit = '';
        $this->orderby = '';
        $this->where = '';

        $this->queriedFields = '*';
    }

    public function apply()
    {
        return $this->getWhere() . $this->getOrderBy() . $this->getLimit();
    }
}