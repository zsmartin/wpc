<?php

namespace WPC\Models;

class User extends \WP_User
{
    /**
     *
     * @return \static
     */
    public static function getCurrent()
    {
        return wp_get_current_user();
    }
    
    public function getMeta($key, $default = '')
    {
        $result = get_user_meta($this->ID, $key, true);
        if (!$result) {
            $result = $default;
        }

        return $result;
    }
}