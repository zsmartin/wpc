<?php

namespace WPC\Models;

abstract class Post
{

    /**
     * @property \WP_Post $wpPost
     */
    public $wpPost;
    public $tags;
    public $isMultipage;
    public $pages;

    protected $cache;
    protected $cachedPost;

    protected $title;
    protected $permalink;
    protected $mainCategory = null;
    protected $mainCategoryLink = null;
    protected $responsiveThumbnail = array();

    protected function __construct($id)
    {
        if (is_null($id)) {
            return;
        }
        if (is_object($id) && isset($id->ID)) {
            $id = $id->ID;
        }

        $this->cache = App()->getCache();
        $this->cachedPost = $this->cache->get('post-' . $id);
        if (false === $this->cachedPost || $this->cachedPost->wpPost->post_status !== "publish") {
            $this->wpPost = get_post($id);
            if (!$this->wpPost) {
                return;
            }
            $this->wpPost->post_content = stripslashes($this->wpPost->post_content);
            $this->cachedPost = new \stdClass();
            $this->cachedPost->wpPost = $this->wpPost;
        } else {
            foreach (get_object_vars($this->cachedPost) as $property => $value) {
                $this->$property = $value;
            }
        }
    }

    public function init()
    {

    }

    public function saveInCache($field, $value)
    {
        if (!isset($this->wpPost) || $this->wpPost->post_status !== "publish") {
            return;
        }
        $this->cachedPost->$field = $value;
        $this->cache->set('post-' . $this->wpPost->ID, $this->cachedPost);
    }

    /**
     *
     * @return \static|boolean
     */
    public static function getCurrent()
    {
        return static::findById(get_the_ID());
    }

    /**
     *
     * @param int $id
     * @return \static|boolean
     */
    public static function findById($id = null)
    {
        $p = new static($id);
        $p->init();
        if (is_null($p->wpPost) ||
                (defined(get_called_class() . '::POST_TYPE') &&
                $p->wpPost->post_type !== static::POST_TYPE)
        ) {
            return false;
        } else {
            $content = $p->wpPost->post_content;
            if (false !== strpos($content, '<!--nextpage-->')) {
                $content = str_replace("\n<!--nextpage-->\n", '<!--nextpage-->', $content);
                $content = str_replace("\n<!--nextpage-->", '<!--nextpage-->', $content);
                $content = str_replace("<!--nextpage-->\n", '<!--nextpage-->', $content);
                if (0 === strpos( $content, '<!--nextpage-->')) {
                    $content = substr($content, 15);
                }
                $p->pages = explode('<!--nextpage-->', $content);
            } else {
                $p->pages = array($content);
            }
            return $p;
        }
    }

    public function getTags()
    {
        if (is_array($this->tags)) {
            return $this->tags;
        }

        $tags = get_the_tags($this->wpPost->ID);
        if (!$tags) {
            $tags = array();
        }

        return $this->tags = $tags;
    }

    public function isMultipage()
    {
        return count($this->getPages()) > 1;
    }

    public function getPages()
    {
        return $this->pages;
    }

    public function getTitle()
    {
        if ($this->title) {
            return $this->title;
        }

        $this->title = get_the_title($this->wpPost);
        $this->saveInCache('title', $this->title);
        return $this->title;
    }

    public function getPermalink()
    {
        if ($this->permalink) {
            return $this->permalink;
        }

        $this->permalink = esc_url(get_permalink($this->wpPost));
        $this->saveInCache('permalink', $this->permalink);
        return $this->permalink;
    }

    public function getDate($format)
    {
        return strftime($format, strtotime($this->wpPost->post_date));
    }

    public function getMainCategory()
    {
        if (!is_null($this->mainCategory)) {
            return $this->mainCategory;
        }

        $categories = get_the_category($this->wpPost);
        $this->mainCategory = false;
        foreach ($categories as $cat) {
            if ((int)$cat->parent === 0) {
                $this->mainCategory = $cat;
                break;
            }
        }

        $this->saveInCache('mainCategory', $this->mainCategory);
        return $this->mainCategory;
    }

    public function getMainCategoryName()
    {
        $category = $this->getMainCategory();
        if (false === $category) {
            return '';
        }

        return $category->name;
    }

    public function getMainCategoryLink()
    {
        if (!is_null($this->mainCategoryLink)) {
            return $this->mainCategoryLink;
        }

        $category = $this->getMainCategory();
        if (false === $category) {
            $this->mainCategoryLink = '';
        } else {
            $this->mainCategoryLink = get_category_link($category);
        }
        $this->saveInCache('mainCategoryLink', $this->mainCategoryLink);
        return $this->mainCategoryLink;
    }

    public function getResponsiveThumbnail($size)
    {
        if (isset($this->responsiveThumbnail[$size])) {
            return $this->responsiveThumbnail[$size];
        }

        $rImage = new \WPC\ResponsiveImage();
        $rImage->setImagesByThumbnail($this->wpPost->ID, $size);
        $this->responsiveThumbnail[$size] = $rImage;
        $this->saveInCache('responsiveThumbnail', $this->responsiveThumbnail);
        return $this->responsiveThumbnail[$size];
    }

    public function getMeta($key, $default = '')
    {
        $result = get_post_meta($this->wpPost->ID, $key, true);
        if (!$result) {
            $result = $default;
        }

        return $result;
    }
}