<?php

namespace WPC;

class Base
{
    public static function className()
    {
        return get_called_class();
    }

    public function __construct($config = array())
    {
        if (!empty($config))
            foreach ($config as $name => $value)
                $this->$name = $value;

        $this->init();
    }

    public function init()
    {
    }

    public function __get($name)
    {
        $getter = 'get' . $name;
        if (method_exists($this, $getter))
            return $this->$getter();
        else
            throw new Exception('Ismeretlen mező: ' . get_class($this) . '::' . $name);
    }

    public function __set($name, $value)
    {
        $setter = 'set' . $name;
        if (method_exists($this, $setter))
            $this->$setter($value);
        elseif (method_exists($this, 'get' . $name))
            throw new Exception('Ez a mező nem írható: ' . get_class($this) . '::' . $name);
        else
            throw new UnknownPropertyException('Ismeretlen mező: ' . get_class($this) . '::' . $name);
    }

    public function __isset($name)
    {
        $getter = 'get' . $name;
        if (method_exists($this, $getter))
            return $this->$getter() !== null;
        else
            return false;
    }

    public function __unset($name)
    {
        $setter = 'set' . $name;
        if (method_exists($this, $setter))
            $this->$setter(null);
        elseif (method_exists($this, 'get' . $name))
            throw new InvalidCallException('Ez a mező nem írható: ' . get_class($this) . '::' . $name);
    }

    public function getAttributes()
    {
        $class = new \ReflectionClass($this);
        $attrs = array();
        foreach ($class->getProperties(\ReflectionProperty::IS_PUBLIC) as $property)
        {
            $name = $property->getName();
            if (!$property->isStatic())
                $attrs[$name] = $this->$name;
        }
        return $attrs;
    }

    public function toJson()
    {
        return json_encode($this->getAttributes());
    }
}