<?php

namespace WPC;

use \WPC\Exception;

class JsonResponse
{
    protected static $_statusCodes = array(
        100 => '100 Continue',
        101 => '101 Switching Protocols',
        200 => '200 OK',
        201 => '201 Created',
        202 => '202 Accepted',
        203 => '203 Non-Authoritative Information',
        204 => '204 No Content',
        205 => '205 Reset Content',
        206 => '206 Partial Content',
        300 => '300 Multiple Choices',
        301 => '301 Moved Permanently',
        302 => '302 Found',
        303 => '303 See Other',
        304 => '304 Not Modified',
        305 => '305 Use Proxy',
        307 => '307 Temporary Redirect',
        400 => '400 Bad Request',
        401 => '401 Unauthorized',
        402 => '402 Payment Required',
        403 => '403 Forbidden',
        404 => '404 Not Found',
        405 => '405 Method Not Allowed',
        406 => '406 Not Acceptable',
        407 => '407 Proxy Authentication Required',
        408 => '408 Request Timeout',
        409 => '409 Conflict',
        410 => '410 Gone',
        411 => '411 Length Required',
        412 => '412 Precondition Failed',
        413 => '413 Request Entity Too Large',
        414 => '414 Request-URI Too Long',
        415 => '415 Unsupported Media Type',
        416 => '416 Requested Range Not Satisfiable',
        417 => '417 Expectation Failed',
        500 => '500 Internal Server Error',
        501 => '501 Not Implemented',
        502 => '502 Bad Gateway',
        503 => '503 Service Unavailable',
        504 => '504 Gateway Timeout',
        505 => '505 HTTP Version Not Supported'
    );

    public $contentType;
    public static $validOrigins = array();

    protected $status;
    protected $success;
    protected $message;

    protected $_data = array();

    public function __construct($status = 200, $message = '', $contentType = 'application/json')
    {
        $this->status = $status;
        $this->message = $message;
        $this->contentType = $contentType;
    }

    public function __set($name, $value)
    {
        if ($name === 'status') {
            if (false === filter_var($value, FILTER_VALIDATE_INT, array('options' => array('min_range' => 100, 'max_range' => 600)))) {
                throw new Exception('Érvénytelen HTTP státuszkód: ' . $value);
            }

            $this->status = $value;
            $this->success = $this->status < 400;
        } elseif ($name === 'message') {
            $this->message = $value;
        } else {
            $this->_data[$name] = $value;
        }
    }

    public function __get($name)
    {
        if (in_array($name, array('status', 'success', 'message'))) {
            return $this->$name;
        }

        return $this->_data[$name];
    }

    public function __isset($name)
    {
        return isset($this->_data[$name]);
    }

    public function __unset($name)
    {
        unset($this->_data[$name]);
    }

    public function reset()
    {
        $this->_data = array();
        $this->status = 200;
    }

    public function setData(array $data = null)
    {
        $this->_data = $data;
    }

    public function addData(array $data = null)
    {
        $this->_data += $data;
    }

    public function send()
    {
        header('HTTP/1.1 ' . self::getStatusCode($this->status));
        header('Content-Type: ' . $this->contentType);
        if (isset($_SERVER['HTTP_ORIGIN']) && in_array($_SERVER['HTTP_ORIGIN'], self::$validOrigins)) {
            header('Access-Control-Allow-Origin: *');
        }

        $response = array('data' => $this->_data);

        $response['message'] = $this->message;

        if ($this->isSuccess()) {
            $response['success'] = true;
        } else {
            $response['error'] = true;
        }

        echo json_encode($response);

        exit();
    }

    public function isSuccess()
    {
        if (!isset($this->success)) {
            $this->success = $this->status < 400;
        }

        return $this->success;
    }

    public static function getStatusCode($statusCode = 200)
    {
        if (in_array($statusCode, self::$_statusCodes)) {
            return self::$_statusCodes[$statusCode];
        } else {
            return self::$_statusCodes[200];
        }
    }
}
