<?php

namespace WPC;

abstract class Widget extends \WP_Widget
{
    function __construct( $id_base = false, $name = null, $widget_options = array(), $control_options = array() )
    {
        if (!$id_base)
            $id_base = strtolower(array_pop(explode('\\', get_class($this))));
        if (is_null($name))
        {
            if (isset($this->name))
                $name = $this->name;
            else
                $name = array_pop(explode('\\', get_class($this)));
        }
        if ($this->description)
            $widget_options['description'] = $this->description;
        parent::__construct($id_base, $name, $widget_options, $control_options);
    }

    public function getCacheKey($args, $instance)
    {
        return get_class($this) . '-' . md5(json_encode($args)) . '-' . md5(json_encode($instance));
    }

    public function widget($args, $instance)
    {
        if ($this->beforeRender() === false)
            return;

        if (method_exists($this, 'accessFilter') && false === $this->accessFilter($args, $instance))
            return;

        $cache = App()->getCache();
        $key = !empty($instance['cache-key']) ? $instance['cache-key'] : $this->getCacheKey($args, $instance);
        $widget = $cache->get($key);
        if (false === $widget)
        {
            $widget = $this->generate($args, $instance);
            $timeout = !empty($instance['cache-timeout']) ? (int)$instance['cache-timeout'] : 0;
            $cache->set($key, $widget, $timeout);
        }

        echo $widget;
    }

    public abstract function generate($args, $instance);

    public function beforeRender()
    {
        return true;
    }

    public function form($instance)
    {
        if (method_exists($this, 'getForm'))
            $form = $this->getForm($instance);
        else
            $form = '';

        $key = !empty($instance['cache-key']) ? $instance['cache-key'] : $this->getCacheKey($args, $instance);
        $timeout = !empty($instance['cache-timeout']) ? (int)$instance['cache-timeout'] : 0;
        $widget = $this;

        $template = new Template('Widgets.default.form');
        $template->set(compact('widget', 'key', 'timeout'));
        $default = $template->render();

        echo $form . $default;
    }

    public function update($newInstance, $oldInstance)
    {
        $instance = $oldInstance;
        $instance['cache-key'] = !empty($newInstance['cache-key']) ? $newInstance['cache-key'] : '';
        $instance['cache-timeout'] = !empty($newInstance['cache-timeout']) ? (int)$newInstance['cache-timeout'] : 0;

        if (method_exists($this, 'setInstance'))
            $instance = $this->setInstance($instance, $newInstance);

        return $instance;
    }
}