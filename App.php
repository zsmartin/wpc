<?php

namespace WPC;

use \WPC\Config;

class App
{
    public $facebookAdapter;
    public $config;
    public $components;
    public $cache;
    public $analytics;
    public $adminManager;

    public function wpInit()
    {
        $this->initializeComponents();

        if (defined('WP_ADMIN') && WP_ADMIN) {
            add_action('init', function() {
                App()->wpAdminInit();
            });

            add_action('admin_init', function() {
                if (defined('DOING_AJAX') && DOING_AJAX) {
                    Ajax\Manager::route();
                }
            });
        }

        $this->afterWpInit();
    }

    public function wpAdminInit()
    {
        if (!defined('DOING_AJAX'))
            $this->getAdminManager()->wpAdminInit();
    }

    public function themeInit()
    {
        $this->afterThemeInit();
    }

    public function afterWpInit()
    {

    }

    public function afterThemeInit()
    {

    }

    public function getRequiredComponents()
    {
        return array(
            'adminManager' => array(
                'class' => '\WPC\Admin\Manager',
                'options' => array(
                    'namespace' => App()->getParam('namespace')
                )
            ),
            'utils' => array(
                'class' => '\WPC\Utils',
                'options' => array()
            ),
            'wp' => array(
                'class' => '\WPC\WP',
                'options' => array()
            )
        );
    }

    public function getConfig($key = null)
    {
        return new Config($key);
    }

    public function setConfig($config)
    {
        $this->config = $config;
    }

    public function getParam($paramName)
    {
        $config = $this->getConfig('params');
        return $config->get($paramName);
    }

    public function getComponent($componentName)
    {
        if (isset($this->components[$componentName])) {
            return $this->components[$componentName];
        }

        $config = $this->getConfig('components.' . $componentName);
        $componentClass = $config->get('class');
        if (is_null($componentClass)) {
            throw new Exception('Invalid component: ' . $componentName);
        }

        $this->components[$componentName] = new $componentClass($config->get('options'));
        return $this->components[$componentName];
    }

    public function setComponent($componentName, $class, $options = array())
    {
        return $this->components[$componentName] = new $class($options);
    }

    public function initializeComponents()
    {
        foreach ($this->getRequiredComponents() as $name => $component) {
            $this->setComponent($name, $component['class'], $component['options']);
        }

        $components = $this->getConfig('components')->get();
        foreach ($components as $name => $component) {
            if (isset($component['lazy']) && $component['lazy'] === false) {
                $this->getComponent($name);
            }
        }
    }

    /**
     * @return \WPC\FacebookAdapter
     */
    public function getFacebookAdapter()
    {
        return $this->getComponent('facebook');
    }

    /**
     * @return \WPC\Caches\Memcache
     */
    public function getCache()
    {
        $cache = App()->getComponent('cache');
        if (!$cache) {
            $cache = $this->setComponent('cache', '\WPC\Caches\Dummy');
        }

        return $cache;
    }

    /**
     *
     * @return \WPC\Admin\Manager
     */
    public function getAdminManager()
    {
        return $this->getComponent('adminManager');
    }

    /**
     * @return \wpdb
     */
    public function getDb()
    {
        return $GLOBALS['wpdb'];
    }

    /**
     *
     * @return \WPC\Analytics\Google
     */
    public function getAnalytics()
    {
        return $this->getComponent('analytics');
    }

    /**
     *
     * @return \WPC\Utils
     */
    public function getUtils()
    {
        return $this->getComponent('utils');
    }

}