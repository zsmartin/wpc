<?php

namespace WPC;

use \WPC\Template;

class Utils extends \WPC\Component
{
    public $jsVars = array();

    public function init()
    {
        date_default_timezone_set('Europe/Budapest');
        setlocale(LC_ALL, 'hu_HU.utf8');
        $this->registerJsVars();
        $this->addNextPageQuickTag();
    }

    public function addModal($name, $data)
    {
        add_action('wp_footer', function() use ($name, $data) {
            $template = new Template('Modals.' . $name);
            $template->set($data);
            $template->render(true);
        });
    }

    public function parseJsVars()
    {
        $tmpVars = self::mergeArrays(App()->getConfig('jsVars')->get(), $this->jsVars);
        array_walk_recursive($tmpVars, function(&$item) {
            if ($item instanceof \Closure) {
                $item = $item();
            }
        });

        return $tmpVars;
    }

    public function addJsVar($name, $value)
    {
        $this->jsVars[$name] = $value;
    }

    public function addJsVars($vars)
    {
        if (!is_array($vars)) {
            $vars = array($vars);
        }

        foreach ($vars as $name => $value) {
            $this->addJsVar($name, $value);
        }
    }

    public function registerJsVars()
    {
        $self = $this;
        add_action('wp_footer', function () use ($self) {
            ?>
            <script type="text/javascript">
                var App = <?php echo json_encode($self->parseJsVars()); ?>
            </script>
            <?php
        }, 1);
    }

    public function isAjaxRequest()
    {
        return (isset($_SERVER['HTTP_X_REQUESTED_WITH']) &&
            $_SERVER['HTTP_X_REQUESTED_WITH'] === 'XMLHttpRequest') ||
            isset($_GET['AJAX_REQUEST']);
    }

    public function addNextPageQuickTag()
    {
        add_filter('mce_buttons', function($buttons) {
            $pos = array_search('wp_more', $buttons, true);
            if ($pos !== false) {
                $tmp = array_slice($buttons, 0, $pos + 1);
                $tmp[] = 'wp_page';
                $buttons = array_merge($tmp, array_slice($buttons, $pos + 1));
            }
            return $buttons;
        });
    }

    public static function mergeArrays($a, $b)
    {
        $args = func_get_args();
        $res = array_shift($args);
        while (!empty($args)) {
            $next = array_shift($args);
            foreach ($next as $k => $v) {
                if (is_integer($k)) {
                    isset($res[$k]) ? $res[]=$v : $res[$k]=$v;
                }
                elseif (is_array($v) && isset($res[$k]) && is_array($res[$k])) {
                    $res[$k] = self::mergeArrays($res[$k], $v);
                }
                else {
                    $res[$k]=$v;
                }
            }
        }
        return $res;
    }
}