<?php

namespace WPC;

class Component 
{
    public function __construct($options = null) 
    {
        if (!is_null($options)) {
            $this->setOptions($options);
        }
        
        $this->init();
    }
    
    public function init()
    {
        
    }
    
    public function setOption($option, $value) 
    {
        if (method_exists($this, 'set' . ucfirst($option))) {
            $this->{'set' . ucfirst($option)}($value);
        } elseif (property_exists($this, $option)) {
            $this->$option = $value;
        } else {
            throw new Exception('Invalid property "' . $option . '" in "' . __CLASS__ . '"');
        }
    }
    
    public function setOptions(array $options)
    {
        foreach ($options as $option => $value) {
            $this->setOption($option, $value);
        }
    }
}