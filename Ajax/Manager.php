<?php

namespace WPC\Ajax;

use \WPC\Ajax\Exception;

class Manager
{
    public static function route()
    {
        $type = mb_strtolower($_SERVER['REQUEST_METHOD']);
        if (!in_array($type, array('get', 'post'))) {
            return false;
        }

        $data = 'get' === $type ? $_GET : $_POST;

        if (false === strpos($data['action'], '.')) {
            return false;
        }

        list($class, $method) = explode('.', $data['action']);
        $classname = implode('\\', array(App()->getParam('namespace'), 'Ajax', ucfirst($class)));
        $method = $type . ucfirst($method);

        if (class_exists($classname) && is_subclass_of($classname, '\\WPC\\Ajax\\Request')) {
            $object = new $classname();
            $object->response = new \WPC\JsonResponse();
            try{
                $object->access = array_merge(array('logged_in' => array(), 'admin' => array()), $object->access);
                if (in_array($method, array_merge($object->access['logged_in'], $object->access['admin'])) && !is_user_logged_in()) {
                    throw new Exception('Ennek a funkciónak a használatához előbb jelentkezz be!', 401);
                }
                if (in_array($method, $object->access['admin']) && !current_user_can('manage_options')) {
                    throw new Exception('Ennek a funkciónak a használatához admin jogosultság szükséges!', 403);
                }

                $object->$method($data);

            } catch (Exception $e) {
                $object->response->status = $e->getCode();
                $object->response->message = $e->getMessage();
                $object->response->setData($e->getData());

                $object->response->send();
            }
            die(0);
        }
        return false;
    }
}