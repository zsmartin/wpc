<?php

namespace WPC\Ajax;

class Exception extends \WPC\Exception
{
    protected $_data;

    public function __construct($message=null, $code=null, $data = null, $previous=null) {

        if (isset($data))
            $this->setData($data);

        parent::__construct($message, $code, $previous);
    }

    public function getData()
    {
        return $this->_data;
    }

    public function setData($data)
    {
        $this->_data = $data;
    }
}