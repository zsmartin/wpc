<?php

namespace WPC\Ajax;

class Request
{
    /** @property \WPC\JsonResponse $response */
    public $response;
    public $errors;
    public $access = array();
    public $data = array();
    public $inputs = array();

    public function isValid($removeNotSafe = false, $method = null)
    {
        if (is_null($method))

        $this->validate($method, $removeNotSafe);

        return 0 === count($this->errors['invalid']);
    }

    public function validate($method, $removeNotSafe = false)
    {
        $this->errors = array('not_safe' => array(), 'invalid' => array(), 'filtered' => array());

        if (!isset($this->inputs[$method]))
        {
            foreach ($this->data as $field => $value)
                $this->errors['not_safe'][] = $field;

            if ($removeNotSafe)
                $this->data = array();

            return true;
        }

        $inputs = $this->inputs[$method];
        foreach ($this->data as $field => $value)
        {
            if (!in_array($field, array_keys($inputs)))
            {
                $this->errors['not_safe'][] = $field;
                if ($removeNotSafe)
                    unset($this->data[$field]);
            }
            else
            {
                if (isset($inputs[$field]['filter']))
                {
                    switch ($inputs[$field]['filter'])
                    {
                        case 'plaintext':
                            $newValue = strip_tags($value);
                            break;
                        case 'int':
                            $newValue = (int)$value;
                            break;
                        default:
                            $methodName = 'filter' . ucfirst($value['filter']);
                            if (method_exists($this, $methodName))
                                $newValue = $this->$methodName($value);
                    }
                    if ($newValue !== $value)
                    {
                        $this->data[$field] = $newValue;
                        $this->errors['filtered'][] = $field;
                    }
                }
                if (isset($inputs[$field]['validator']))
                {
                    $validator = $inputs[$field]['validator'];
                    $result = filter_var($value, $validator['type'], array('options' => $validator['options']));
                    if (false === $result)
                    {
                        $this->errors['invalid'][] = $field;
                    }
                }
            }
        }
    }
}