<?php

/**
 *
 * @param string $field
 * @param mixed $value
 * @return \WPC\Models\User
 */
function get_user_by( $field, $value ) {
	$userdata = \WP_User::get_data_by( $field, $value );

	if (!$userdata) {
		return false;
    }

	$namespace = App()->getParam('namespace');
    $userClass = "\\{$namespace}\\Models\\User";
    if (class_exists($userClass)) {
        $user = new $userClass();
    } else {
        $user = new \WPC\Models\User();
    }
	$user->init($userdata);

	return $user;
}

/**
 *
 * @global \WPC\Models\User $current_user
 * @param int $id
 * @param string $name
 * @return \WPC\Models\User
 */
function wp_set_current_user($id, $name = '') {
	global $current_user;

	if (isset($current_user) && ($current_user instanceof \WPC\Models\User) && ($id == $current_user->ID)) {
		return $current_user;
    }

	$namespace = App()->getParam('namespace');
    $userClass = "\\{$namespace}\\Models\\User";
    if (class_exists($userClass)) {
        $current_user = new $userClass($id, $name);
    } else {
        $current_user = new \WPC\Models\User($id, $name);
    }
	setup_userdata($current_user->ID);

	do_action('set_current_user');

	return $current_user;
}

function wp_get_current_user() {
	global $current_user;

	get_currentuserinfo();

    $current_user = apply_filters('wpc:get_current_user', $current_user);

	return $current_user;
}