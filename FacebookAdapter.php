<?php

namespace WPC;

use \WPC\Component,
    \WPC\Facebook\SDK,
    \WPC\Facebook\GraphMeta;

class FacebookAdapter extends Component
{
    public $appId;
    public $fanpageId;
    public $fanpageUrl;
    public $secret;
    public $useFbAuth = true;

    /**
     *
     * @var \WPC\Facebook\SDK
     */
    public $sdk;
    public $graphMeta;

    public function init()
    {
        $this->sdk = new SDK(array('appId' => $this->appId, 'secret' => $this->secret));
        $this->graphMeta = new GraphMeta(array('app_id' => $this->appId));
        $this->sdk->addToSite();

        if ($this->useFbAuth) {
            add_filter('wpc:get_current_user', array($this, 'addFbAuth'));
        }
    }

    /**
     *
     * @param \WPC\Models\User $currentUser
     * @return boolean
     */
    public function addFbAuth($currentUser)
    {
        if (!$currentUser->exists()) {
            $this->sdk->authenticate();
        }

        return $GLOBALS['current_user'];
    }
}
