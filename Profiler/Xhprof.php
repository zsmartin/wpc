<?php

namespace WPC\Profiler;

use \WPC\Component,
    \WPC\Exception;

class Xhprof extends Component
{
    public $conditions = array();
    public $useDefaultConditions = true;

    public $basePath;
    public $flags = XHPROF_FLAGS_NO_BUILTINS;

    public $useManualRecording = false;

    protected $enabled = true;
    protected $started = false;
    protected $resultLink;

    public $data;

    public function init()
    {
        if (!extension_loaded('xhprof')) {
            throw new Exception('Xhprof extension not loaded!');
        }

        if (!is_dir($this->basePath) || !is_readable($this->basePath . '/config.php')) {
            throw new Exception('Invalid base path: ' . $this->basePath);
        }

        if ($this->useDefaultConditions) {
            $this->conditions = array_merge($this->getDefaultConditions(), $this->conditions);
        }

        if (!$this->useManualRecording) {
            if ($this->start()) {
                add_action('shutdown', array($this, 'onShutdown'));
            }
        }
    }

    public function onShutdown()
    {
        try {
            $this->end();
            $this->save();
        } catch (Exception $e) {
            error_log($e->getMessage() . '[' . $e->getFile() . ':' . $e->getLine() . ']');
        }
    }

    public function isValid()
    {
        foreach ($this->conditions as $cond) {
            if ($cond !== true) {
                return false;
            }
        }

        return true;
    }

    public function start()
    {
        try {
            if ($this->isValid() && !$this->isStarted() && $this->isEnabled()) {
                xhprof_enable($this->flags);
                $this->started = true;
            }
        } catch (Exception $e) {
            error_log($e->getMessage() . '[' . $e->getFile() . ':' . $e->getLine() . ']');
        }

        return $this->started === true;
    }

    public function end()
    {
        if (!$this->isStarted()) {
            throw new Exception('Xhprof session not yet started');
        }

        $this->data = xhprof_disable();
        $this->started = false;
    }

    public function isStarted()
    {
        return $this->started === true;
    }

    public function save()
    {
        if (!$this->data) {
            throw new Exception('There are no recorded Xhprof sessions');
        }

        $appNs = App()->getParam('namespace');
        $namespace = $appNs . '.' . APPLICATION_ENV;

        defined('XHPROF_LIB_ROOT') || define('XHPROF_LIB_ROOT', $this->basePath);
        require_once XHPROF_LIB_ROOT . '/config.php';
        require_once XHPROF_LIB_ROOT . '/utils/xhprof_lib.php';
        require_once XHPROF_LIB_ROOT . '/utils/xhprof_runs.php';

        $xhprofRuns = new \XHProfRuns_Default();
        $xhprofRuns->save_run($this->data, $namespace);
    }

    public function enable()
    {
        $this->enabled = true;
    }

    public function disable()
    {
        $this->enabled = false;
    }

    public function isEnabled()
    {
        return $this->enabled === true;
    }

    public function getDefaultConditions()
    {
        return array(
            !isset($_POST['action']) || $_POST['action'] !== 'heartbeat'
        );
    }
}