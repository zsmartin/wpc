<?php

namespace WPC\Api;

use \WPC\Component;

class Controller extends Component
{
    public $defaultAction = 'index';
    public $format = 'json';

    public $action;
    public $sendBody = true;

    public $resourceId;
    public $resourceFilters = [];

    public function init()
    {
        $this->setResourceFilters($this->resourceFilters);
    }

    public function defaultSettings()
    {
        return [];
    }

    public function fields()
    {
        return [];
    }

    public function setResourceFilters($filters)
    {
        $this->resourceFilters = array_merge($this->defaultSettings(), $filters);
    }

    public function runAction($actionName = null)
    {
        if (is_null($actionName)) {
            $actionName = $this->defaultAction;
        }

        if (!method_exists($this, $actionName . 'Action')) {
            header(':', 405);
            die();
        }

        $this->action = $actionName;
        $result = call_user_func([$this, $actionName . 'Action']);
        if (!is_array($result)) {
            $result = [$result];
        }
        $filteredResult = [];
        foreach ($result as $record) {
            $filteredResult[] = $this->transformFields($record);
        }

        $response = $this->formatOutput($filteredResult);

        header('Allow: ' . implode(', ', $this->getSupportedVerbs()));

        if (strtolower($_SERVER['REQUEST_METHOD']) !== 'head') {
            echo $response;
        }
        die();
    }

    public function getSupportedVerbs()
    {
        $verbs = ['OPTIONS'];
        if (isset($this->resourceId)) {
            if (method_exists($this, 'viewAction')) {
                $verbs = ['GET', 'HEAD'];
            }
            if (method_exists($this, 'updateAction')) {
                $verbs = ['PUT'];
            }
            if (method_exists($this, 'deleteAction')) {
                $verbs = ['DELETE'];
            }
        } else {
            if (method_exists($this, 'indexAction')) {
                $verbs = ['GET', 'HEAD'];
            }
            if (method_exists($this, 'createAction')) {
                $verbs = ['POST'];
            }
        }

        $verbs = array_merge($verbs, ['OPTIONS']);

        return $verbs;
    }

    public function optionsAction()
    {
        return $this->getSupportedVerbs();
    }

    protected function formatOutput($data, $format = null)
    {
        if (is_null($format)) {
            $format = $this->format;
        }

        $methodName = 'formatAs' . ucfirst($format);
        if (method_exists($this, $methodName)) {
            return $this->$methodName($data);
        } else {
            return false;
        }
    }

    protected function formatAsJson($data)
    {
        header('Content-Type: application/json');

        return json_encode($data);
    }

    protected function formatAsXml($data)
    {
        header('Content-Type: text/xml');

        $writer = new \XMLWriter;
        $writer->openMemory();
        $writer->setIndent(true);
        $writer->setIndentString("    ");
        $writer->startDocument('1.0', 'UTF-8');
        $writer->startElement('posts');
        foreach ($data as $post) {
            $writer->startElement('post');
            foreach (get_object_vars($post) as $name => $value) {
                $writer->startElement($name);
                $writer->startCdata();
                $writer->text($value);
                $writer->endCdata();
                $writer->endElement();
            }
            $writer->endElement();
        }
        $writer->endElement();
        $writer->endDocument();

        return $writer->outputMemory(true);
    }

    protected function transformFields($resource)
    {
        $result = [];
        $fields = explode(',', $this->resourceFilters['fields']);
        foreach ($fields as $field) {
            $field = trim($field);
            if (!isset($this->fields()[$field])) {
                continue;
            }

            $returnValue = ($this->fields()[$field]);
            if (is_string($returnValue)) {
                $value = $resource->$returnValue;
            } else if (is_callable($returnValue)) {
                $value = $returnValue($resource);
            }

            $result[$field] = $this->htmlEntityDecodeDeep($value);

        }

        return $result;
    }

    protected function htmlEntityDecodeDeep($data)
    {
        if (is_string($data)) {
            $data = html_entity_decode($data);
        } else if (is_array($data) || is_object($data)) {
            foreach ($data as &$value) {
                $value = $this->htmlEntityDecodeDeep($value);
            }
        }

        return $data;
    }
}