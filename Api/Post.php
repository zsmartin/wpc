<?php

namespace WPC\Api;

use \WPC\Api\Controller;

class Post extends Controller
{
    public function defaultSettings()
    {
        return [
            'category' => '',
            'topic' => '',
            'tag' => '',
            'before' => false,
            'after' => false,
            'count' => 10,
            'q' => ''
        ];
    }

    public function fields()
    {
        return [
            'id' => 'ID',
            'title' => 'post_title',
            'lead' => function ($post) {
                return \Player\Models\Post::findById($post->ID)->getLead();
            },
            'content' => 'post_content',
            'date' => 'post_date',
            'thumbnail' => function ($post) {
                return wp_get_attachment_image_src(get_post_thumbnail_id($post->ID))[0];
            },
            'category' => function ($post) {
                $categories = get_the_terms($post->ID, 'category');
                if (!$categories) {
                    return [];
                }

                $newCats = [];
                foreach ($categories as $cat) {
                    $newCats[] = ['slug' => $cat->slug, 'name' => $cat->name];
                }
                return $newCats;
            },
            'topic' => function ($post) {
                $topics = get_the_terms($post->ID, 'topic');
                if (!$topics) {
                    return [];
                }

                $newTopics = [];
                foreach ($topics as $topic) {
                    $newTopics[] = ['slug' => $topic->slug, 'name' => $topic->name];
                }
                return $newTopics;
            },
            'tag' => function ($post) {
                $tags = get_the_tags($post->ID);
                if (!$tags) {
                    return [];
                }

                $newTags = [];
                foreach ($tags as $tag) {
                    $newTags[] = ['slug' => $tag->slug, 'name' => $tag->name];
                }
                return $newTags;
            },
            'author' => function ($post) {
                $author = get_user_by('id', $post->post_author);
                return [['slug' => $author->user_nicename, 'name' => $author->display_name]];
            },
            'permalink' => function ($post) {
                return get_permalink($post->ID);
            }
        ];
    }

    public function whereFilter($where)
    {
        if (!$this->resourceFilters['before'] && !$this->resourceFilters['after']) {
            return $where;
        }

        if ($this->resourceFilters['before']) {
            $tag = 'ID < ' . (int)$this->resourceFilters['before'];
        } else {
            $tag = 'ID > ' . (int)$this->resourceFilters['after'];
        }

        $where .= " AND {$tag} ";
        return $where;
    }

    public function indexAction()
    {
        $args = [
            'posts_per_page' => $this->resourceFilters['count'],
            'suppress_filters' => false
        ];

        if ($this->resourceFilters['q']) {
            $args['s'] = $this->resourceFilters['q'];
        }

        $tax = false;
        if ($this->resourceFilters['category']) {
            $tax = 'category';
            $term = $this->resourceFilters['category'];
        } else if ($this->resourceFilters['topic']) {
            $tax = 'topic';
            $term = $this->resourceFilters['topic'];
        } else if ($this->resourceFilters['tag']) {
            $tax = 'post_tag';
            $term = $this->resourceFilters['tag'];
        }

        if ($tax) {
            $args['tax_query'] = [
                [
                    'taxonomy' => $tax,
                    'field' => 'slug',
                    'terms' => $term
                ]
            ];
        }

        add_filter('posts_where', array($this, 'whereFilter'));
        $posts = get_posts($args);
        remove_filter('posts_where', array($this, 'whereFilter'));

        if (!isset($this->resourceFilters['fields']) || !$this->resourceFilters['fields']) {
            $this->resourceFilters['fields'] = 'id,title,date,thumbnail,category,topic,tag,author,permalink';
        }

        return $posts;
    }

    public function viewAction()
    {
        $post = \Player\Models\Post::findById($this->resourceId);
        if (!$post) {
            header(':', true, 404);
            die();
        }

        if (!isset($this->resourceFilters['fields']) || !$this->resourceFilters['fields']) {
            $this->resourceFilters['fields'] = 'id,title,lead,content,date,category,topic,tag,author,permalink';
        }

        return $post->wpPost;
    }
}