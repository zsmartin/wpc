<?php

namespace WPC\Api;

use \WPC\Api\Controller;

class Category extends Controller
{
    public function fields()
    {
        return [
            'slug' => 'slug',
            'name' => 'name',
            'description' => 'description',
            'count' => 'count',
            'parent' => function ($term) {
                if (!$term->parent) {
                    return '-';
                }
                $parent = get_term($term->parent, 'category');
                return $parent ? $parent->name : '-';
            }
        ];
    }

    public function indexAction()
    {
        if (!isset($this->resourceFilters['fields']) || !$this->resourceFilters['fields']) {
            $this->resourceFilters['fields'] = 'slug,name,description,count,parent';
        }

        return array_values(get_terms('category'));
    }
}