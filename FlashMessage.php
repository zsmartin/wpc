<?php

namespace WPC;

class FlashMessage
{
    public function __construct($message, $level = 'info') {
        add_action('admin_notices', function(){
            $template = new Template('Partials.flashmessage');
            $template->set('class', ('error' == $level) ? 'error' : 'updated');
            $template->set('message', $message);
            $template->render(true);
        });
    }
}