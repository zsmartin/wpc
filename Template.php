<?php

namespace WPC;

class Template
{
    public $filename;
    protected $_data = array();

    protected $_displayContents = false;

    public function __construct($name)
    {
        $this->filename = APPLICATION_PATH . '/views/' . wp_get_theme()->template . '/' . join('/', explode('.', $name)) . '.php';

        if (!is_readable($this->filename)) {
            $fallback = FRAMEWORK_PATH . '/Views/' . join('/', explode('.', $name)) . '.php';
            if (!is_readable($fallback)) {
                throw new Exception("Ismeretlen template: {$name} (forrás: {$this->filename})");
            }
            $this->filename = $fallback;
        }
    }

    public function set($name, $value = null)
    {
        if (is_array($name))
        {
            foreach ($name as $n => $v)
                $this->_data[$n] = $v;
        }
        else
            $this->_data[$name] = $value;
    }

    public function get($name)
    {
        return $this->_data[$name];
    }

    public function remove($name)
    {
        unset($this->_data[$name]);
    }

    public function clear()
    {
        $this->_data = array();
    }

    public function render($display = false)
    {
        $this->_displayContents = $display;
        extract($this->_data);
        ob_start();
        include $this->filename;
        $contents = ob_get_contents();
        ob_end_clean();

        if ($this->_displayContents)
        {
            echo $contents;
            return true;
        }
        else
            return $contents;
    }
}