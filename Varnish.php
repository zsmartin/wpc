<?php

namespace WPC;

use \WPC\Component,
    \WPC\Exception;

class Varnish extends Component
{
    const METHOD_REQUEST = 'request';
    const METHOD_CLI = 'cli';

    public $method = self::METHOD_CLI;
    public $cliOptions = array();
    protected $cliCommand = null;

    public function init()
    {
        add_action('save_post', function($postId) {
            if (!isset($_REQUEST['post_type']) ||
                    $_REQUEST['post_type'] !== 'post' ||
                    !isset($_REQUEST['post_status']) ||
                    $_REQUEST['post_status'] !== 'publish') {
                return;
            }

            $this->purgePost($postId);
        });
    }

    public function getCliCommand()
    {
        if (!is_null($this->cliCommand)) {
            return $this->cliCommand;
        }

        if (!isset($this->cliOptions['path']) || empty($this->cliOptions['path'])) {
            throw new Exception("Varnish: No CLI path given");
        }

        if (!file_exists($this->cliOptions['path'])) {
            throw new Exception("Varnish: CLI path not exists");
        }

        $command = $this->cliOptions['path'];

        if (isset($this->cliOptions['host']) && !empty($this->cliOptions['host'])) {
            $command .= " -T {$this->cliOptions['host']}";
            if (isset($this->cliOptions['port'])) {
                $command .= ":{$this->cliOptions['port']}";
            }
        }

        if (isset($this->cliOptions['secret']) && !empty($this->cliOptions['secret']) && file_exists($this->cliOptions['secret'])) {
            $command .= " -S {$this->cliOptions['secret']}";
        }

        if (isset($this->cliOptions['name']) && !empty($this->cliOptions['name'])) {
            $command .= " -n {$this->cliOptions['name']}";
        }

        if (isset($this->cliOptions['timeout']) && (int)$this->cliOptions['timeout'] > 0) {
            $timeout = (int)$this->cliOptions['timeout'];
            $command .= " -t {$timeout}";
        }

        return $this->cliCommand = escapeshellcmd($command);
    }

    public function purgePost($postId)
    {
        $post = get_post($postId);
        $urls = array('~' . get_permalink($post), site_url());
        $categories = get_the_category($postId);
        if (is_array($categories)) {
            foreach ($categories as $cat) {
                $urls[] = get_category_link($cat);
            }
        }
        $tags = get_the_tags($postId);
        if (is_array($tags)) {
            foreach ($tags as $tag) {
                $urls[] = get_tag_link($tag);
            }
        }
        $topics = get_the_terms($post, 'topic');
        if (is_array($topics)) {
            foreach ($topics as $topic) {
                $urls[] = get_term_link($topic, 'topic');
            }
        }
        $series = get_the_terms($post, 'series');
        if (is_array($series)) {
            foreach ($series as $s) {
                $urls[] = get_term_link($s, 'series');
            }
        }

        if ($this->method === self::METHOD_REQUEST) {
            $this->purgeUrls($urls);
        } else {
            $this->banUrls($urls);
        }
    }

    public function purgeHome()
    {
        $this->purgeUrls(site_url());
    }

    public function purgeUrls($urls)
    {
        ignore_user_abort(1);
        foreach ($urls as $url) {
            if (strpos($url, '~') === 0) {
                $url = substr($url, 1);
                $method = 'regex';
            } else {
                $method = 'fixed';
            }
            $host = parse_url($url, PHP_URL_HOST);
            wp_remote_request($url, array(
                'method' => 'PURGE',
                'headers' => array(
                    'host' => $host,
                    'X-Purge-Method' => $method
                ),
                'timeout' => 0.01,
                'blocking' => false
            ));
        }
    }

    public function banUrls($urls)
    {
        $command = $this->getCliCommand();
        foreach ($urls as $url) {
            if (strpos($url, '~') === 0) {
                $url = substr($url, 1);
                $operator = '~';
                $parts = parse_url($url);
                $url = $parts['path'];
                if (strrpos($url, '/') === strlen($url) - 1) {
                    $url = substr($url, 0, -1);
                }
            } else {
                $operator = '==';
                $parts = parse_url($url);
                $url = $parts['path'];
            }
            if (strlen(trim($url)) === 0) {
                $url = '/';
            }
            system("{$command} \"ban req.http.host == {$parts['host']} && req.url {$operator} {$url}\"");
        }
    }
}